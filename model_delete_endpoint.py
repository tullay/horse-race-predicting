import boto3
from time import gmtime, strftime
from sagemaker.amazon.amazon_estimator import get_image_uri
import time
import os
import yaml

region = 'ap-southeast-2'

directory = os.path.dirname(os.path.abspath(__file__))
filename = os.path.join(directory, '..', 'training_configurations', sys.argv[1])
with open(filename, 'r') as stream:
    try:
        config = yaml.load(stream)
    except yaml.YAMLError as exc:
        print(exc)

training_job_name = config['training_job_name']

client = boto3.client('sagemaker', region_name=region)

print(client.delete_endpoint(EndpointName=training_job_name))
