import json
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.externals import joblib
import math
import numpy
import csv
from random import randint
import bisect
import pprint
import re
import pickle
from datetime import datetime, timedelta
from time import gmtime, strftime
import encoder_helper
import os
import boto3
import re
from sagemaker import get_execution_role
import io
import random
import urllib.request
from sagemaker.amazon.amazon_estimator import get_image_uri
from sagemaker import get_execution_role
from time import gmtime, strftime
import requests

CSV_FILE_NAME = 'data/data_raw'
the_list = []
MODEL_ID = 'horseracing-xgboost-regression-' + strftime("%Y-%m-%d-%H-%M-%S", gmtime())

# Training Statics
role = 'arn:aws:iam::928290690802:role/service-role/AmazonSageMaker-ExecutionRole-20180413T170070'
region = 'ap-southeast-2'

bucket='tullys-models' # put your s3 bucket name here, and create s3 bucket
s3_prefix = 'sagemaker/xgboost-horseracing'
# customize to your bucket where you have stored the data
bucket_path = 'https://s3-{}.amazonaws.com/{}'.format(region,bucket)

FILE_DATA = 'data/data_raw_formatted_with_zeros.csv'

def main():
    global the_list

    with open(CSV_FILE_NAME + '.json', mode='r', encoding='utf-8') as f:
        the_list = json.loads(f.read())

    the_list = clean_object_strings(the_list)

    # TODO Fix with better random
    for i in range(1):
        print("Rotating horse stats count[{}]".format(i))
        the_list = rotate_horse_stats2(the_list)

    the_list = add_winner(the_list)

    # print(the_list)

    # print(the_list)
    df = pd.read_json(json.dumps(the_list))
    df.sort_index(axis=1, inplace=True)
    col = "winning_number"
    df = pd.concat([df[col],df.drop(col,axis=1)], axis=1)

    df.to_csv(CSV_FILE_NAME + '.csv', header=True, index=False)

    ###
    # Drop data the model wont have at prediction time
    ###
    df = drop_tables(df)

    # Save for analysis
    df.to_csv(CSV_FILE_NAME + '_formatted_with_headers' + '.csv', header=True, index=False)
    df.to_csv(CSV_FILE_NAME + '_formatted' + '.csv', header=False, index=False)
    ###
    # Add zeros to missing data as per Xgboost
    ###
    df.replace('', 0, inplace=True)

    ###
    # Encode all strings to numbers
    ###
    dataset = df.values
    dataset = encode_lables(df, dataset)
    df2 = pd.DataFrame(dataset)
    df2.columns = df.columns.values.tolist()

    # Output
    df2.to_csv(CSV_FILE_NAME + '_formatted_with_zeros' + '.csv', header=False, index=False)

    print('Formatting Completed Successfully')
    input()
    train()
    print('Training Completed Successfully')
    with open('config.yaml', mode='w', encoding='utf-8') as f:
        f.write('model_id: ' + MODEL_ID)

def add_winner(the_list_local):
    new_list = []
    # record['winning_number']
    for idx, row in enumerate(the_list_local):
        winning_number_found = False
        winning_number = 1
        for column_header in the_list_local[idx].keys():
            if column_header.find('finish_') != -1:
                # print(column_header, the_list_local[idx][column_header])
                if the_list_local[idx][column_header] is '1':
                    print('Adding winning horse number: ', column_header.split('_')[-1])
                    the_list_local[idx]['winning_number'] = column_header.split('_')[-1]
                    new_list.append(the_list_local[idx])
                    winning_number_found = True
                    break
        if winning_number_found is False:
            print(type(the_list_local))
            print('Winner not found at[{}]'.format(idx))
    return new_list


def rotate_horse_stats2(the_list_local):
    max = 0

    ###
    # Calcualte the max
    ###
    df = pd.read_json(json.dumps(the_list_local))
    for column_header in df.columns.values.tolist():
        if hasNumbers(column_header) and int(re.findall('\d+', column_header)[0]) > max:
            max = int(re.findall('\d+', column_header)[0])

    # print('Number Indexes = ', max) #TODO write to file
    with open('max_indexes.txt', mode='w', encoding='utf-8') as f:
        f.write(str(max))

    features_to_swap = []

    for column_header in df.columns.values.tolist():
        if(hasNumbers(column_header)):
            column_header_stripped = ''.join([i for i in column_header if not i.isdigit()])
            if column_header_stripped not in features_to_swap:
                features_to_swap.append(column_header_stripped)
                # print("features_to_swap added: ", column_header_stripped)
    # print(features_to_swap)
    # exit()
    for idx, row in enumerate(the_list_local):
        # print('Shuffling row[{}]'.format(idx))
        for count in range(max):
            # print('count: ', count)
            number_1 = count + 1
            number_2 = randint(1, max)
            # print('Swapping ({}, {})'.format(number_1, number_2))
            if number_1 == number_2: # Cant swap
                # print('Cant swap ({}, {})'.format(number_1, number_2))
                continue
            for feature in features_to_swap:
                columnsTitles=[feature + str(number_1), feature + str(number_2)]
                if columnsTitles[0] not in the_list_local[idx]:
                    the_list_local[idx][columnsTitles[0]] = 0
                if columnsTitles[1] not in the_list_local[idx]:
                    the_list_local[idx][columnsTitles[1]] = 0
                # print('row[{}] Before: columnsTitles0[{}]: {} columnsTitles1[{}]: {}'.format(idx, columnsTitles[0], the_list_local[idx][columnsTitles[0]], columnsTitles[1], the_list_local[idx][columnsTitles[1]]))
                temp_obj = the_list_local[idx][columnsTitles[0]]
                the_list_local[idx][columnsTitles[0]] = the_list_local[idx][columnsTitles[1]]
                the_list_local[idx][columnsTitles[1]] = temp_obj
                # print('row[{}] After: columnsTitles0[{}]: {} columnsTitles1[{}]: {}'.format(idx, columnsTitles[0], the_list_local[idx][columnsTitles[0]], columnsTitles[1], the_list_local[idx][columnsTitles[1]]))
                # print("Swapped: ", columnsTitles[0], ' <-> ', columnsTitles[1])
    # exit()
    # print(the_list_local)
    return the_list_local

def drop_tables(df):
    print("Dropping Tables..")
    for idx, value in enumerate(df.columns.values.tolist()):
        features_to_drop = ['margin_', 'finish_', 'date', 'starting_price']
        for feature in features_to_drop:
            if(str(value).find(feature) != -1):
                print("Dropping: ", value)
                df = df.drop(columns=[value])
    return df

def encode_lables(df, dataset):
    features_to_encode = ['winning_number', 'breeder_', 'jockey_', 'horse_name_', 'track_location', 'track_name', 'trainer_', 'track_type']
    print("Encoding lables..")
    print('Creating Encoders..')
    for feature in features_to_encode:
        temp_list = []
        for idx, value in enumerate(df.columns.values.tolist()):
            if(str(value).find(feature) != -1):
                print('Found appeding: ', feature)
                temp_list.extend(dataset[:,idx])
        # print(temp_list)
        temp_list.append('<unknown>')
        # if '0' not in temp_list:
        #     temp_list.append('0')
        label_encoder = LabelEncoder()
        label_encoder = label_encoder.fit(temp_list)
        print('Num Classes[', feature, ']: ', len(label_encoder.classes_))
        encoder_helper.save_label_encoder(MODEL_ID, label_encoder, feature)
        # numpy.save('encoders/' + feature + '_' + datetime.strftime(datetime.now(), '%Y-%m-%d-%H-%M-%S') + '.npy', label_encoder.classes_)
        # with open('encoders/' + feature + '_' + datetime.strftime(datetime.now(), '%Y-%m-%d-%H-%M-%S') + '.pickle', 'wb') as f:
        #     # Pickle the 'data' dictionary using the highest protocol available.
        #     pickle.dump(label_encoder, f, pickle.HIGHEST_PROTOCOL)

    for idx, value in enumerate(df.columns.values.tolist()):
        for feature in features_to_encode:
            if(str(value).find(feature) != -1):
                print("Encoding: ", value)
                # label_encoder = LabelEncoder()
                # label_encoder.classes_ = numpy.load('encoders/' + feature + '_' + datetime.strftime(datetime.now(), '%Y-%m-%d-%H-%M-%S') + '.npy')
                label_encoder = encoder_helper.load_label_encoder('encoders/{}/{}.pickle'.format(MODEL_ID, feature))
                dataset[:,idx] = label_encoder.transform(dataset[:,idx].astype(str))
                # print(dataset[:,idx].astype(str))
    return dataset


def clean_object_strings(dict_array):
    for idx, record in enumerate(dict_array):
        for key, value in record.items():
            # print(key, ': ', value)
            if value is None:
                dict_array[idx][key] = 0
                # print('added: ', dict_array[idx][key])
            if(str(value).find('$') != -1):
                dict_array[idx][key] = str(dict_array[idx][key]).replace('$', '')
                # print('cleaned: ', dict_array[idx][key])
            if(str(key).find('weight_') != -1):
                dict_array[idx][key] = str(dict_array[idx][key]).replace('kg', '')
                dict_array[idx][key] = str(dict_array[idx][key]).split(' ')[0]
                # print('cleaned: ', dict_array[idx][key])
            if(str(key).find('margin_') != -1):
                dict_array[idx][key] = str(dict_array[idx][key]).replace('L', '')
                # print('cleaned: ', dict_array[idx][key])
            if(str(key).find('starting_price_') != -1):
                dict_array[idx][key] = str(dict_array[idx][key]).replace('F', '')
                # print('cleaned: ', dict_array[idx][key])
            if(str(key).find('winning_number') != -1):
                dict_array[idx][key] = str(dict_array[idx][key]).replace('e', '')
                # print('cleaned[winning_number]: ', dict_array[idx][key])
            if(str(key).find('number_') != -1):
                dict_array[idx][key] = str(dict_array[idx][key]).replace('e', '')
                # print('cleaned[winning_number]: ', dict_array[idx][key])
            if(str(key).find('finish_') != -1):
                dict_array[idx][key] = str(dict_array[idx][key]).replace('F', '')
                # print('cleaned[winning_number]: ', dict_array[idx][key])

    return dict_array

def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

###
# Training
###
def train():
    if os.path.exists(os.path.join(os.path.expanduser('~'), '.aws')):
        boto3.setup_default_session(profile_name='tully')

    #split the downloaded data into train/test/validation files
    FILE_TRAIN = FILE_DATA + '.train.csv'
    FILE_VALIDATION = FILE_DATA + '.validation.csv'
    FILE_TEST = FILE_DATA + '.test.csv'
    PERCENT_TRAIN = 70
    PERCENT_VALIDATION = 15
    PERCENT_TEST = 15
    data_split(FILE_DATA, FILE_TRAIN, FILE_VALIDATION, FILE_TEST, PERCENT_TRAIN, PERCENT_VALIDATION, PERCENT_TEST)

    #upload the files to the S3 bucket
    upload_to_s3(bucket, 'train', FILE_TRAIN)
    upload_to_s3(bucket, 'test', FILE_TEST)
    upload_to_s3(bucket, 'validation', FILE_VALIDATION)



    container = get_image_uri(region, 'xgboost')

    job_name = MODEL_ID
    print("Training job", job_name)

    #Ensure that the training and validation data folders generated above are reflected in the "InputDataConfig" parameter below.

    with open('max_indexes.txt', mode='r', encoding='utf-8') as f:
        num_classes = f.read()

    create_training_params = \
    {
        "AlgorithmSpecification": {
            "TrainingImage": container,
            "TrainingInputMode": "File"
        },
        "RoleArn": role,
        "OutputDataConfig": {
            "S3OutputPath": bucket_path + "/" + s3_prefix + "/single-xgboost"
        },
        "ResourceConfig": {
            "InstanceCount": 1,
            "InstanceType": "ml.m4.xlarge",
            "VolumeSizeInGB": 5
        },
        "TrainingJobName": job_name,
        "HyperParameters": {
            "max_depth":"6",
            "subsample":"0.7",
            "silent":"0",
            "objective":"multi:softprob",
            "num_class":str(num_classes),
            "num_round":"100"
        },
        "StoppingCondition": {
            "MaxRuntimeInSeconds": 3600
        },
        "InputDataConfig": [
            {
                "ChannelName": "train",
                "DataSource": {
                    "S3DataSource": {
                        "S3DataType": "S3Prefix",
                        "S3Uri": bucket_path + "/" + s3_prefix + '/train',
                        "S3DataDistributionType": "FullyReplicated"
                    }
                },
                "ContentType": "text/csv",
                "CompressionType": "None"
            },
            {
                "ChannelName": "validation",
                "DataSource": {
                    "S3DataSource": {
                        "S3DataType": "S3Prefix",
                        "S3Uri": bucket_path + "/" + s3_prefix + '/validation',
                        "S3DataDistributionType": "FullyReplicated"
                    }
                },
                "ContentType": "text/csv",
                "CompressionType": "None"
            }
        ]
    }


    client = boto3.client('sagemaker', region_name=region)
    client.create_training_job(**create_training_params)

    import time

    status = client.describe_training_job(TrainingJobName=job_name)['TrainingJobStatus']
    print(status)
    while status !='Completed' and status!='Failed':
        time.sleep(60)
        status = client.describe_training_job(TrainingJobName=job_name)['TrainingJobStatus']
        print(status)

def data_split(FILE_DATA, FILE_TRAIN, FILE_VALIDATION, FILE_TEST, PERCENT_TRAIN, PERCENT_VALIDATION, PERCENT_TEST):
    data = [l for l in open(FILE_DATA, 'r')]
    train_file = open(FILE_TRAIN, 'w')
    valid_file = open(FILE_VALIDATION, 'w')
    tests_file = open(FILE_TEST, 'w')

    num_of_data = len(data)
    num_train = int((PERCENT_TRAIN/100.0)*num_of_data)
    num_tests = int((PERCENT_TEST/100.0)*num_of_data)
    num_valid = int((PERCENT_VALIDATION/100.0)*num_of_data)

    data_fractions = [num_train, num_tests, num_valid]
    split_data = [[],[],[]]

    rand_data_ind = 0

    for split_ind, fraction in enumerate(data_fractions):
        for i in range(fraction):
            rand_data_ind = random.randint(0, len(data)-1)
            split_data[split_ind].append(data[rand_data_ind])
            data.pop(rand_data_ind)

    for l in split_data[0]:
        train_file.write(l)

    for l in split_data[1]:
        valid_file.write(l)

    for l in split_data[2]:
        tests_file.write(l)

    train_file.close()
    valid_file.close()
    tests_file.close()

def write_to_s3(fobj, bucket, key):
    return boto3.Session().resource('s3').Bucket(bucket).Object(key).upload_fileobj(fobj)

def upload_to_s3(bucket, channel, filename):
    fobj=open(filename, 'rb')
    key = s3_prefix+'/'+channel
    url = 's3://{}/{}/{}'.format(bucket, key, filename)
    print('Writing to {}'.format(url))
    write_to_s3(fobj, bucket, key)


main()
