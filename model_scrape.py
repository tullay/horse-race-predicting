from lxml import html
import requests
import pandas as pd
from enum import Enum
import json
import time
import datetime
import os
import unicodedata
import re
import pprint
import base64

STATE_CODES = ['NSW', 'VIC', 'QLD', 'WA', 'SA', 'ACT', 'TAS', 'NT', ]

BASE_URL = 'https://www.racingaustralia.horse'
# https://www.racingaustralia.horse/FreeFields/Calendar_Results.aspx?State=STATE_CODE
LIST_RESULTS_URL = 'https://www.racingaustralia.horse/FreeFields/Calendar_Results.aspx?State={state}'
LIST_RESULTS_XPATH = "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-fields']/tbody/tr/td[2]/a"

ALL_FORM_URL = "https://www.racingaustralia.horse/FreeFields/AllForm.aspx?Key{key}"
ALL_FORM_ALL_HORSES_XPATH = "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='horse-form-table']"
# https://www.racingaustralia.horse/FreeFields/Results.aspx?Key=YYYYMONDD,STATE_CODE,LOCATION
DAY_RESULTS_URL = 'https://www.racingaustralia.horse/FreeFields/Results.aspx?Key='

CSV_FILE_NAME = 'data/data_raw'
COMPLETED_LINKS_FILENAME = 'data/completed_links.txt'

RACE_FEATURES = ['number', 'finish', 'margin', 'starting_price']

RACE_FEATURE_XPATHS = {
    'number': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[3]",
    'finish': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[2]/span",
    'margin': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[7]",
    'starting_price': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[11]"
}

ALL_FORM_RACE_FEATURES = ['horse_name', 'trainer', 'jockey', 'hcp_rating', 'barrier', 'weight']

ALL_FORM_RACE_FEATURE_XPATHS = {
    'number': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[@class='no']",
    'horse_name': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[@class='horse']/a",
    'trainer': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[@class='trainer']/a",
    'jockey': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[@class='jockey']/a",
    'barrier': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[@class='barrier']",
    'weight': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[@class='weight']",
    'hcp_rating': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[@class='hcp']",
}

TRACK_LOCATION_XPATH = "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/div[@class='race-venue']/div[@class='top']/h2"

TRACK_DETAILS_XPATH = "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-title'][{race_number}]/tbody/tr[@class='race-info']/td"

RACE_COUNT_XPATH = "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/div[@id='race-number-menu']/div/a"

RACE_TABLE_EVEN_ODDS_RESULTS = [None, 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow']
RACE_TABLE_EVEN_ODDS_ALL_FORM = [None, 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow']
RACE_TABLE_IDS = [None, '1', '1', '2', '2', '3', '3', '4', '4', '5', '5', '6', '6', '7', '7', '8', '8', '9', '9', '10', '10', '11', '11']

the_list = []
completed_links = []

def main():
    global the_list
    global completed_links

    if os.path.exists(COMPLETED_LINKS_FILENAME):
        with open(COMPLETED_LINKS_FILENAME, mode='r', encoding='utf-8') as f:
            completed_links = f.readlines()
        # you may also want to remove whitespace characters like `\n` at the end of each line
        completed_links = [x.strip() for x in completed_links]

    if os.path.exists(CSV_FILE_NAME + '.json'):
        with open(CSV_FILE_NAME + '.json', mode='r', encoding='utf-8') as f:
            the_list = json.loads(f.read())

    # print(the_list)
    # exit()

    for state in STATE_CODES:
        print('Processing state [{}]..'.format(state))
        results_urls = get_result_urls(state)
        # print(results_urls)
        for url in results_urls:
            print("Checking URL: " + url)
            if url.find('Trial') != -1:
                print("Skipping.. Trial race")
                continue
            if url in completed_links:
                print("Skipping.. Completed link")
                continue
            get_results(url)
            time.sleep(1.5)
    print("Completed Successfully")


def get_result_urls(state):
    result = []

    page = requests.get(LIST_RESULTS_URL.format(state=state))
    tree = html.fromstring(page.content)

    for path in tree.xpath(clean_xpath(LIST_RESULTS_XPATH)):
        result.append(str(BASE_URL + str(path.get('href'))))
    return result

def get_results(url):
    results_page = requests.get(url)

    archive_page(url, results_page.content)

    if str(results_page.content).find('Results for this race are not currently available') != -1:
        print('Results for this race are not currently available')
        return

    results_page_tree = html.fromstring(results_page.content)

    # print('allformurl: ' + ALL_FORM_URL.format(key=url.split(sep='Key', maxsplit=1)[1]))
    all_form_url = ALL_FORM_URL.format(key=url.split(sep='Key', maxsplit=1)[1])

    all_form_page = requests.get(all_form_url)

    archive_page(all_form_url, all_form_page.content)

    all_form_page_tree = html.fromstring(all_form_page.content)

    race_count = results_page_tree.xpath(clean_xpath(RACE_COUNT_XPATH))
    print("Race count: " + str(len(race_count) - 1))


    # current_race = 1

    for current_race in range(len(race_count) - 1):
        current_race = current_race + 1
        print("Processing places race [{race_number}]..".format(race_number=current_race))
        temp_obj = {}
        index = 1

        ###
        # Get Track info
        ###
        track_loction = all_form_page_tree.xpath(clean_xpath(TRACK_LOCATION_XPATH))
        if(len(track_loction) > 0):
            normalized_track_loction = unicodedata.normalize("NFKD", track_loction[0].text_content())
            # print(normalized_track_loction)
            temp_obj['track_location'] = normalized_track_loction.split(':')[0].strip()
            temp_obj['date'] = normalized_track_loction.split('\n')[2].replace('\n', '').replace('\r', '').strip()

        track_details = all_form_page_tree.xpath(clean_xpath(TRACK_DETAILS_XPATH.format(race_number=current_race)))
        if(len(track_details) > 0):
            normalized_track_details = unicodedata.normalize("NFKD", track_details[0].text_content())
            # print(normalized_track_details)
            # print(re.search("(?:Track Type: +)(\w+)(?: +Field)", normalized_track_details).group(1))
            if(re.search('(?:Track Type: +)(\w+)(?: +Field)', normalized_track_details) != None):
                temp_obj['track_type'] = re.search('(?:Track Type: +)(\w+)(?: +Field)', normalized_track_details).group(1)
            if(re.search('(?:Track Name: +)(\w+)(?: +Track Type)', normalized_track_details) != None):
                temp_obj['track_name'] = re.search('(?:Track Name: +)(\w+)(?: +Track Type)', normalized_track_details).group(1)

        ###
        # Process results page
        # Gets horse number and winner, etc.
        ###
        while(True):
            for feature in RACE_FEATURES:
                xpath = ''
                xpath = RACE_FEATURE_XPATHS[feature].format(race_number=current_race, number=index)
                xpath = clean_xpath(xpath)
                res = results_page_tree.xpath(xpath)[0].text_content() if len(results_page_tree.xpath(xpath)) > 0 else None
                print('Processing feature[{}] result[{}]'.format(feature, res))
                temp_obj['{feature}_{index}'.format(feature=feature, index=index)] = res

            index = index + 1
            try:
                results_page_tree.xpath(clean_xpath(str(RACE_FEATURE_XPATHS['number'].format(race_number=current_race, number=index))))[0]
            except Exception as e:
                print("No more horses in race results")
                print(e)
                break

        ###
        # Process all form page
        # Get most of the stats for the horse.
        ###
        # print(list(temp_obj))
        # pprint.pprint(temp_obj)
        # input()
        for key in list(temp_obj):
            if key.find('number_') != -1:

                existing_index = key.split('_')[1]
                existing_horse_number = temp_obj[key]
                index = 1
                print('Existing index: ', existing_index)

                if existing_horse_number is None:
                    continue

                while(True):
                    ###
                    # Find pre-existing horse number index by index
                    ###
                    xpath = ALL_FORM_RACE_FEATURE_XPATHS['number'].format(race_number=current_race, number=index)
                    xpath = clean_xpath(xpath)
                    # print(xpath)
                    current_horse_number = all_form_page_tree.xpath(xpath)[0].text_content()

                    print('Existing horse number[{}] Current horse number[{}]'.format(existing_horse_number, current_horse_number))
                    if existing_horse_number == current_horse_number:
                        print('Index[{}] Scraping all form for horse numbered[{}]'.format(index, current_horse_number))
                        # Scrape the data for the horse
                        for feature in ALL_FORM_RACE_FEATURES:
                            xpath = ''
                            xpath = ALL_FORM_RACE_FEATURE_XPATHS[feature].format(race_number=current_race, number=index)
                            xpath = clean_xpath(xpath)
                            res = all_form_page_tree.xpath(xpath)[0].text_content() if len(all_form_page_tree.xpath(xpath)) > 0 else None
                            print('Processing feature[{}] result[{}] with xpath: {}'.format(feature, res, xpath))
                            temp_obj['{feature}_{index}'.format(feature=feature, index=existing_index)] = res
                            # if feature == 'horse_name':
                            #     input()

                        print("Scraping stats for horse at index[{}] named[{}]".format(index, temp_obj['horse_name_'+str(existing_index)]))
                        # Get per horse info
                        array_of_horse_infos = all_form_page_tree.xpath(clean_xpath(ALL_FORM_ALL_HORSES_XPATH))
                        for horse_info in array_of_horse_infos:
                            # print("Horse info", horse_info.text_content())
                            normalized = unicodedata.normalize("NFKD", horse_info.text_content())
                            cleaned = [x.strip() for x in normalized.strip().replace('\t', '').replace('\r', '').split('\n') if x]
                            # print(cleaned)
                            if cleaned[1].find(temp_obj['horse_name_{index}'.format(index=existing_index)]) != -1:
                                for element in cleaned:
                                    if element.find(' year old') != -1:
                                        temp_obj['age_{index}'.format(index=existing_index)] = re.findall('\d+', element)[0]
                                    if element.find('Prizemoney') != -1:
                                        temp_obj['prizemoney_{index}'.format(index=existing_index)] = re.findall('\d+,?\d*,?\d*,?\d*', element)[0].replace(',', '')
                                    if element.find('Record:') != -1:
                                        temp_obj['starts_{index}'.format(index=existing_index)] = re.findall('\d+', element)[0]
                                        temp_obj['firsts_{index}'.format(index=existing_index)] = re.findall('\d+', element)[1]
                                        temp_obj['seconds_{index}'.format(index=existing_index)] = re.findall('\d+', element)[2]
                                        temp_obj['thirds_{index}'.format(index=existing_index)] = re.findall('\d+', element)[3]
                                    if element.find('1st Up:') != -1:
                                        temp_obj['first_up_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('1st Up: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['first_up_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('1st Up: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['first_up_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('1st Up: \d+:\d+-\d+-\d+', element)[0])[3]
                                        temp_obj['first_up_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('1st Up: \d+:\d+-\d+-\d+', element)[0])[4]
                                    if element.find('2nd Up:') != -1:
                                        temp_obj['second_up_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('2nd Up: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['second_up_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('2nd Up: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['second_up_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('2nd Up: \d+:\d+-\d+-\d+', element)[0])[3]
                                        temp_obj['second_up_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('2nd Up: \d+:\d+-\d+-\d+', element)[0])[4]
                                    if element.find('Track:') != -1:
                                        temp_obj['track_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['track_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['track_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['track_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Dist:') != -1:
                                        temp_obj['dist_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Dist: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['dist_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Dist: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['dist_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Dist: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['dist_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Dist: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Track/Dist:') != -1:
                                        temp_obj['track_dist_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track/Dist: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['track_dist_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track/Dist: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['track_dist_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track/Dist: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['track_dist_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track/Dist: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Firm:') != -1:
                                        temp_obj['firm_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Firm: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['firm_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Firm: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['firm_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Firm: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['firm_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Firm: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Good:') != -1:
                                        temp_obj['good_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Good: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['good_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Good: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['good_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Good: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['good_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Good: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Soft:') != -1:
                                        temp_obj['soft_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Soft: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['soft_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Soft: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['soft_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Soft: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['soft_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Soft: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Heavy:') != -1:
                                        temp_obj['heavy_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Heavy: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['heavy_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Heavy: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['heavy_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Heavy: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['heavy_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Heavy: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Synthetic:') != -1:
                                        temp_obj['synthetic_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Synthetic: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['synthetic_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Synthetic: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['synthetic_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Synthetic: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['synthetic_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Synthetic: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Breeder:') != -1:
                                        temp_obj['breeder_{index}'.format(index=existing_index)] = element.split(':', maxsplit=1)[1]
                                    if element.find('Owners:') != -1:
                                        temp_obj['breeder_{index}'.format(index=existing_index)] = element.split(':', maxsplit=1)[1]
                        break
                    index = index + 1
                    try:
                        all_form_page_tree.xpath(clean_xpath(str(ALL_FORM_RACE_FEATURE_XPATHS['number'].format(race_number=current_race, number=index))))[0]
                    except Exception as e:
                        print("No more horses in race all form")
                        print(e)
                        break
        # print(temp_obj)
        # input()
        the_list.append(temp_obj)

            # print(the_list)


        # current_race = current_race + 1
    # print(the_list)
    with open(CSV_FILE_NAME + '.json', 'w+') as out:
        out.write(json.dumps(the_list))
    pd.read_json(json.dumps(the_list)).to_csv(CSV_FILE_NAME + '.csv')

    with open(COMPLETED_LINKS_FILENAME, mode='a+', encoding='utf-8') as myfile:
        myfile.write(url + '\n')

def archive_page(url, pagecontent):
    print('Archiving webpage')
    base64_encoded_url = base64.b64encode(url.encode())
    with open('webpage_archive/'+base64_encoded_url.decode(), 'wb') as f:
        f.write(pagecontent)

def clean_xpath(xpath):
    return xpath.replace("tbody", '')

main()
