#!/bin/bash
python3.6 -m pip install boto3==1.9.29 lxml==4.2.5 sagemaker==1.11.3 pandas==0.23.4 scikit-learn==0.19.2 numpy==1.15.1 requests==2.18.4 && \
aws s3 sync s3://tullys-models/code/ . && \
echo syncing data from S3 && \
aws s3 sync s3://tullys-models/data ./data && \
echo running model_scrape.py && \
python3.6 model_scrape.py && \
echo syncing data to S3 && \
aws s3 sync ./data s3://tullys-models/data && \
echo syncing webpage_archive to S3 && \
aws s3 sync ./webpage_archive s3://tullys-models/webpage_archive && \
echo running model_format.py && \
python3.6 model_format.py && \
echo synching encoders && \
aws s3 sync ./encoders/ s3://tullys-models/encoders/ && \
echo syncing configs && \
aws s3 sync ./configs s3://tullys-models/configs && \
python3.6 model_create_endpoint.py && \
echo running model_validate.py && \
python3.6 model_validate.py && \
echo syncing confidence results && \
aws s3 sync ./confidence_results s3://tullys-horse-racing-public/confidence_results ; \
echo running model_delete_endpoint.py ; \
python3.6 model_delete_endpoint.py
