import boto3
from time import gmtime, strftime
from sagemaker.amazon.amazon_estimator import get_image_uri
import time
import pandas as pd
import os
import yaml

role = 'arn:aws:iam::928290690802:role/service-role/AmazonSageMaker-ExecutionRole-20180413T170070'
region = 'ap-southeast-2'

bucket='tullys-models' # put your s3 bucket name here, and create s3 bucket
prefix = 'sagemaker/xgboost-horseracing'
# customize to your bucket where you have stored the data
bucket_path = 'https://s3-{}.amazonaws.com/{}'.format(region, bucket)

FILE_TEST = 'data/data_raw_formatted_with_zeros.csv.test.csv'

with open('config.yaml', 'r') as file:
    config = yaml.load(file)
model_id = config['model_id']

if os.path.exists(os.path.join(os.path.expanduser('~'), '.aws')):
    boto3.setup_default_session(profile_name='tully')

client = boto3.client('sagemaker', region_name=region)

container = get_image_uri(region, 'xgboost')
endpoint_config_name = model_id

###
# Test
###
runtime_client = boto3.client('runtime.sagemaker', region_name='ap-southeast-2')

import json
from itertools import islice
import math
import struct
import csv

file_name = FILE_TEST #customize to your test file
# with open(file_name, 'r') as f:
#     payload = f.read().strip()

file_path = 'confidence_results/confidence_result_'+ model_id +'.csv'

def main():
    calc_results_matrix(get_results(file_name))

def calc_results_matrix(results_list):
    print('Calculating Results')
    # results_list = []
    for num in range(100):
        if num < 10:
            num = float('0.0'+str(num))
        else:
            num = float('0.'+str(num))

        total = 0
        correct_predictions = 0
        accuracy = 0

        for result in results_list:
            print('Processing result')
            winning_class = 0
            winning_confidence = 0
            for idx, res in enumerate(result['prediction_list']):
                if float("{:.8f}".format(float(res))) > float(winning_confidence):
                    winning_confidence = res
                    winning_class = idx

            if float(winning_confidence) > float(num):
                if total > 0 and correct_predictions > 0:
                    accuracy = float(correct_predictions)/float(total)
                # print('Race[{}]\t\tReal[{}] \t\tPredicted[{}]\t\tConfidence[{}]\t\tRunning accuracy[{}]'.format(idx2, real_winner_class, winning_class, winning_confidence, accuracy))
                total = total + 1
                if int(result['real_winner_class']) == int(winning_class):
                    correct_predictions = correct_predictions + 1

        if correct_predictions > 0:
            accuracy = float(correct_predictions)/float(total)
        else:
            accuracy = 0
        string = (str(num)+','+str(total)+','+str(correct_predictions)+','+str(accuracy))+'\n'
        print(string)
        # results_list.append(string)
        with open(file_path, mode='a+', encoding='utf-8') as fo:
            fo.write(string)

def get_results(csv_file_name):
    print('Getting results')
    result_list = []
    with open(csv_file_name, mode='r', encoding='utf-8') as f2:
        row_reader = csv.reader(f2)

        df_formatted = pd.read_csv(csv_file_name)

        total = 0
        for row_idx, row in df_formatted.iterrows():
            # temp_row_dict = {}
            row2 = row.values.tolist()
            real_winner_class = row2[0]
            row2.pop(0)

            print('Getting results for row[{}]'.format(row_idx))
            payload = str(row2).replace('[', '').replace(']', '')

            # print('Payload', payload)
            response = runtime_client.invoke_endpoint(EndpointName=model_id,
                                               ContentType='text/csv',
                                               Body=payload)
            result = response['Body'].read()
            result = result.decode("utf-8")
            # print(result)

            temp_result_list = str(result).replace('[', '').replace(']', '').replace(',', '').split(' ')
            # print(result_dict)
            temp_dict = {'real_winner_class': real_winner_class, 'prediction_list': temp_result_list}
            result_list.append(temp_dict)
    return result_list

main()
