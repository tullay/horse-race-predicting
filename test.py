from sklearn.preprocessing import LabelEncoder
import numpy
import pickle

encodername = 'encoders/winning_number'

label_encoder = LabelEncoder()
label_encoder.classes_ = numpy.load(encodername+'.npy')
# print(label_encoder.inverse_transform(int(16)))
# input(1)

with open(encodername+'.pickle', 'wb') as f:
    # Pickle the 'data' dictionary using the highest protocol available.
    pickle.dump(label_encoder, f, pickle.HIGHEST_PROTOCOL)

# # label_encoder2 = LabelEncoder()
#
# with open(encodername+'.pickle', 'rb') as f:
#     label_encoder2 = pickle.load(f)
#
# input(2)
# print(label_encoder2.inverse_transform(int(16)))
