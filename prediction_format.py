import json
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.externals import joblib
import math
import numpy
import csv
from random import randint
import pprint
import os
import encoder_helper
import yaml

# CSV_FILE_NAME = 'out_future/future_races'
OUT_LOCATION = 'out_future/'
FINAL_OUT_LOCATION = 'out_future_formatted/'
CSV_FILES_LOCATION = 'out_future_scraped'

with open('config.yaml', 'r') as file:
    config = yaml.load(file)
model_id = config['model_id']

the_list = []

max_indexes = 0

def main():
    global the_list
    global max_indexes

    with open('max_indexes.txt', mode='r', encoding='utf-8') as f:
        max_indexes = int(f.read())

    for filename in os.listdir(CSV_FILES_LOCATION):
        if filename.endswith(".json"):
            print('Formatting filename[{}]'.format(CSV_FILES_LOCATION+'/'+filename.split('.json')[0]))

            trimmmed_filename = filename.split('.json')[0]
            csv_file = CSV_FILES_LOCATION+'/'+filename

            with open(csv_file, mode='r', encoding='utf-8') as f:
                the_list = json.loads(f.read())

            the_list = clean_object_strings(the_list)
            df = pd.read_json(json.dumps(the_list))
            df.to_csv(OUT_LOCATION + trimmmed_filename + '2.csv', header=True, index=False)

            ###
            # Drop data model wont have a prediction time
            ###
            df = drop_tables(df)
            df = add_missing_indexes(df)
            df.sort_index(axis=1, inplace=True)
            # input()

            df.to_csv(OUT_LOCATION + trimmmed_filename  + '_original_with_headers' + '.csv', header=True, index=False)

            # print(df.columns.values.tolist())
            dataset = df.values



            ###
            # Encode all strings to numbers
            ###
            dataset = encode_lables_from_disk(df, dataset)

            df2 = pd.DataFrame(dataset)
            df2.columns = df.columns.values.tolist()

            ###
            # Rotate stats
            ###
            # df2 = rotate_horse_stats(df2)

            # print(df2)
            # exit()
            df2.to_csv(OUT_LOCATION + trimmmed_filename  + '_cleaned' + '.csv', header=False, index=False)
            df2.to_csv(OUT_LOCATION + trimmmed_filename  + '_cleaned_with_headers' + '.csv', header=True, index=False)

            ###
            # Add zeros
            ###
            with open(OUT_LOCATION + trimmmed_filename  + '_cleaned' + '.csv', mode='r', encoding='utf-8') as f1:
                row_reader = csv.reader(f1)
                with open(FINAL_OUT_LOCATION + trimmmed_filename  + '_cleaned_with_zeros' + '.csv', mode='w', encoding='utf-8') as f2:
                    row_writer = csv.writer(f2)
                    # first_row = next(row_reader)
                    # row_writer.writerow(first_row)
                    for row in row_reader:
                        for idx, val in enumerate(row):
                            # print(val)
                            if val is None:
                                row[idx] = 0
                            elif str(val) == '':
                                row[idx] = 0
                        row_writer.writerow(row)

    print('Completed Successfully')

def add_missing_indexes(df):
    numbered_features = []
    local_df = df

    for column_header in df.columns.values.tolist():
        if(hasNumbers(column_header)):
            column_header_stripped = ''.join([i for i in column_header if not i.isdigit()])
            if column_header_stripped not in numbered_features:
                numbered_features.append(column_header_stripped)

    for num in range(1, max_indexes+1):
        for feature in numbered_features:
            if str(feature+str(num)) not in df.columns:
                local_df[str(feature+str(num))] = 0
    return local_df

def rotate_horse_stats2(the_list_local):
    max = 20
    features_to_swap = []

    for column_header in the_list_local[0].keys():
        if(hasNumbers(column_header)):
            column_header_stripped = ''.join([i for i in column_header if not i.isdigit()])
            if column_header_stripped not in features_to_swap:
                features_to_swap.append(column_header_stripped)
                # print("features_to_swap added: ", column_header_stripped)
    # print(features_to_swap)
    # exit()
    for idx, row in enumerate(the_list_local):
        print('Shuffling row[{}]'.format(idx))
        for count in range(max):
            # print('count: ', count)
            number_1 = count + 1
            number_2 = randint(0, 9) + 1
            # print('Swapping ({}, {})'.format(number_1, number_2))
            if number_1 == number_2: # Cant swap
                # print('Cant swap ({}, {})'.format(number_1, number_2))
                continue
            for feature in features_to_swap:
                columnsTitles=[feature + str(number_1), feature + str(number_2)]
                if columnsTitles[0] not in the_list_local[idx]:
                    the_list_local[idx][columnsTitles[0]] = 0
                if columnsTitles[1] not in the_list_local[idx]:
                    the_list_local[idx][columnsTitles[1]] = 0
                # print('Before: Row[0]: ', the_list_local[idx][columnsTitles[0]], 'Row[1]: ', the_list_local[idx][columnsTitles[1]])
                temp_obj = the_list_local[idx][columnsTitles[0]]
                the_list_local[idx][columnsTitles[0]] = the_list_local[idx][columnsTitles[1]]
                the_list_local[idx][columnsTitles[1]] = temp_obj
                # print('After: Row[0]: ', the_list_local[idx][columnsTitles[0]], 'Row[1]: ', the_list_local[idx][columnsTitles[1]])
                # print("Swapped: ", columnsTitles[0], ' <-> ', columnsTitles[1])
    # print(the_list_local)
    return the_list_local

def drop_tables(df):
    print("Dropping Tables..")
    for idx, value in enumerate(df.columns.values.tolist()):
        features_to_drop = ['margin_', 'finish_', 'date', 'starting_price']
        for feature in features_to_drop:
            if(str(value).find(feature) != -1):
                print("Dropping: ", value)
                df = df.drop(columns=[value])
    return df

def encode_lables_from_disk(df, dataset):
    features_to_encode = ['breeder_', 'jockey_', 'horse_name_', 'track_location', 'track_name', 'trainer_', 'track_type']
    print("Encoding lables..")
    for idx, value in enumerate(df.columns.values.tolist()):
        for feature in features_to_encode:
            if(str(value).find(feature) != -1):
                print("Encoding: ", value)
                label_encoder = encoder_helper.load_label_encoder('encoders/' + model_id + '/' + feature + '.pickle')
                for idx2, item in enumerate(dataset[:,idx]):
                    if isinstance(item, str):
                        try:
                            # print(dataset[idx2][idx])
                            dataset[idx2][idx] = label_encoder.transform([item])[0]
                            # print(dataset[idx2][idx])
                        except Exception as e:
                            print('ERROR', e)
                            try:
                                print(dataset[idx2][idx])
                                dataset[idx2][idx] = label_encoder.transform(['<unknown>'])[0]
                                print(dataset[idx2][idx])
                            except Exception as e:
                                print('Why Error', e)
                                dataset[idx2][idx] = 0
                    else:
                        print('NOT String[{}] setting 0'.format(item))
                        dataset[idx2][idx] = 0
                # print(dataset[:,idx])
                # exit()
                # dataset[:,idx] = label_encoder.transform(dataset[:,idx].astype(str))
                # print(dataset[:,idx].astype(str))
    return dataset


def clean_object_strings(dict_array):
    for idx, record in enumerate(dict_array):
        for key, value in record.items():
            # print(key, ': ', value)
            if value is None:
                dict_array[idx][key] = 0
                # print('added: ', dict_array[idx][key])
            if(str(value).find('$') != -1):
                dict_array[idx][key] = str(dict_array[idx][key]).replace('$', '')
                # print('cleaned: ', dict_array[idx][key])
            if(str(key).find('weight_') != -1):
                dict_array[idx][key] = str(dict_array[idx][key]).replace('kg', '')
                dict_array[idx][key] = str(dict_array[idx][key]).split(' ')[0]
                # print('cleaned: ', dict_array[idx][key])
            if(str(key).find('margin_') != -1):
                dict_array[idx][key] = str(dict_array[idx][key]).replace('L', '')
                # print('cleaned: ', dict_array[idx][key])
            if(str(key).find('starting_price_') != -1):
                dict_array[idx][key] = str(dict_array[idx][key]).replace('F', '')
                # print('cleaned: ', dict_array[idx][key])
            if(str(key).find('winning_number') != -1):
                dict_array[idx][key] = str(dict_array[idx][key]).replace('e', '')
                # print('cleaned[winning_number]: ', dict_array[idx][key])
            if(str(key).find('number_') != -1):
                dict_array[idx][key] = str(dict_array[idx][key]).replace('e', '')
                # print('cleaned[winning_number]: ', dict_array[idx][key])
            if(str(key).find('finish_') != -1):
                dict_array[idx][key] = str(dict_array[idx][key]).replace('F', '')
                # print('cleaned[winning_number]: ', dict_array[idx][key])

    return dict_array

def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

main()
