from lxml import html
import requests
import pandas as pd
from enum import Enum
import json
import time
import datetime
import os
import unicodedata
import re
import pprint
import base64

LINKS_TO_CHECK = []

CSV_FILE_NAME = 'out_future/future_races'

STATE_CODES = ['NSW', 'VIC', 'QLD', 'WA', 'SA', 'ACT', 'TAS', 'NT', ]

BASE_URL = 'https://www.racingaustralia.horse'
# # https://www.racingaustralia.horse/FreeFields/Calendar_Results.aspx?State=STATE_CODE
# LIST_RESULTS_URL = 'https://www.racingaustralia.horse/FreeFields/Calendar_Results.aspx?State={state}'
# LIST_RESULTS_XPATH = "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-fields']/tbody/tr/td[2]/a"

ALL_FORM_URL = "https://www.racingaustralia.horse/FreeFields/AllForm.aspx?Key{key}"
ALL_FORM_ALL_HORSES_XPATH = "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='horse-form-table']"
# https://www.racingaustralia.horse/FreeFields/Results.aspx?Key=YYYYMONDD,STATE_CODE,LOCATION
DAY_RESULTS_URL = 'https://www.racingaustralia.horse/FreeFields/Results.aspx?Key='

GET_TOMORROW_URLS_XPATH = "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/table[@class='full-calendar']/tbody/tr[@class='rows'][1]//a"
ALL_FORM_ENDING = '&recentForm=N'

COMPLETED_LINKS_FILENAME = 'data/completed_links.txt'

# RACE_FEATURES = ['number', 'finish', 'margin', 'starting_price']
#
# RACE_FEATURE_XPATHS = {
#     'number': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[3]",
#     'finish': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[2]/span",
#     'margin': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[7]",
#     'starting_price': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[11]"
# }

ALL_FORM_RACE_FEATURES = ['horse_name', 'trainer', 'jockey', 'hcp_rating', 'barrier', 'weight']

ALL_FORM_RACE_FEATURE_XPATHS = {
    'number': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[@class='no']",
    'horse_name': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[@class='horse']/a",
    'trainer': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[@class='trainer']/a",
    'jockey': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[@class='jockey']/a",
    'barrier': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[@class='barrier']",
    'weight': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[@class='weight']",
    'hcp_rating': "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-strip-fields'][{race_number}]/tbody/tr[contains(@class, 'EvenRow') and not(contains(@class, 'Scratched')) or contains(@class, 'OddRow') and not(contains(@class, 'Scratched'))][{number}]/td[@class='hcp']",
}

TRACK_LOCATION_XPATH = "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/div[@class='race-venue']/div[@class='top']/h2"

TRACK_DETAILS_XPATH = "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/table[@class='race-title'][{race_number}]/tbody/tr[@class='race-info']/td"

RACE_COUNT_XPATH = "/html/body[@class='ShowQuickLinks']/div[@id='main']/div[@id='main-content']/div[@id='middle-container']/div[@id='info-container']/div[@id='grey-content']/div[@id='page-content']/div[@id='race-number-menu']/div/a"

RACE_TABLE_EVEN_ODDS_RESULTS = [None, 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow']
RACE_TABLE_EVEN_ODDS_ALL_FORM = [None, 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow', 'OddRow', 'EvenRow']
RACE_TABLE_IDS = [None, '1', '1', '2', '2', '3', '3', '4', '4', '5', '5', '6', '6', '7', '7', '8', '8', '9', '9', '10', '10', '11', '11']

HEADERS = ['age_1', 'age_10', 'age_11', 'age_12', 'age_13', 'age_14', 'age_15', 'age_16', 'age_17', 'age_18', 'age_19', 'age_2', 'age_20', 'age_3', 'age_4', 'age_5', 'age_6', 'age_7', 'age_8', 'age_9', 'barrier_1', 'barrier_10', 'barrier_11', 'barrier_12', 'barrier_13', 'barrier_14', 'barrier_15', 'barrier_16', 'barrier_17', 'barrier_18', 'barrier_19', 'barrier_2', 'barrier_20', 'barrier_3', 'barrier_4', 'barrier_5', 'barrier_6', 'barrier_7', 'barrier_8', 'barrier_9', 'breeder_1', 'breeder_10', 'breeder_11', 'breeder_12', 'breeder_13', 'breeder_14', 'breeder_15', 'breeder_16', 'breeder_17', 'breeder_18', 'breeder_19', 'breeder_2', 'breeder_20', 'breeder_3', 'breeder_4', 'breeder_5', 'breeder_6', 'breeder_7', 'breeder_8', 'breeder_9', 'date', 'dist_firsts_1', 'dist_firsts_10', 'dist_firsts_11', 'dist_firsts_12', 'dist_firsts_13', 'dist_firsts_14', 'dist_firsts_15', 'dist_firsts_16', 'dist_firsts_17', 'dist_firsts_18', 'dist_firsts_19', 'dist_firsts_2', 'dist_firsts_20', 'dist_firsts_3', 'dist_firsts_4', 'dist_firsts_5', 'dist_firsts_6', 'dist_firsts_7', 'dist_firsts_8', 'dist_firsts_9', 'dist_seconds_1', 'dist_seconds_10', 'dist_seconds_11', 'dist_seconds_12', 'dist_seconds_13', 'dist_seconds_14', 'dist_seconds_15', 'dist_seconds_16', 'dist_seconds_17', 'dist_seconds_18', 'dist_seconds_19', 'dist_seconds_2', 'dist_seconds_20', 'dist_seconds_3', 'dist_seconds_4', 'dist_seconds_5', 'dist_seconds_6', 'dist_seconds_7', 'dist_seconds_8', 'dist_seconds_9', 'dist_starts_1', 'dist_starts_10', 'dist_starts_11', 'dist_starts_12', 'dist_starts_13', 'dist_starts_14', 'dist_starts_15', 'dist_starts_16', 'dist_starts_17', 'dist_starts_18', 'dist_starts_19', 'dist_starts_2', 'dist_starts_20', 'dist_starts_3', 'dist_starts_4', 'dist_starts_5', 'dist_starts_6', 'dist_starts_7', 'dist_starts_8', 'dist_starts_9', 'dist_thirds_1', 'dist_thirds_10', 'dist_thirds_11', 'dist_thirds_12', 'dist_thirds_13', 'dist_thirds_14', 'dist_thirds_15', 'dist_thirds_16', 'dist_thirds_17', 'dist_thirds_18', 'dist_thirds_19', 'dist_thirds_2', 'dist_thirds_20', 'dist_thirds_3', 'dist_thirds_4', 'dist_thirds_5', 'dist_thirds_6', 'dist_thirds_7', 'dist_thirds_8', 'dist_thirds_9', 'finish_1', 'finish_10', 'finish_11', 'finish_12', 'finish_13', 'finish_14', 'finish_15', 'finish_16', 'finish_17', 'finish_18', 'finish_19', 'finish_2', 'finish_20', 'finish_3', 'finish_4', 'finish_5', 'finish_6', 'finish_7', 'finish_8', 'finish_9', 'firm_firsts_1', 'firm_firsts_10', 'firm_firsts_11', 'firm_firsts_12', 'firm_firsts_13', 'firm_firsts_14', 'firm_firsts_15', 'firm_firsts_16', 'firm_firsts_17', 'firm_firsts_18', 'firm_firsts_19', 'firm_firsts_2', 'firm_firsts_20', 'firm_firsts_3', 'firm_firsts_4', 'firm_firsts_5', 'firm_firsts_6', 'firm_firsts_7', 'firm_firsts_8', 'firm_firsts_9', 'firm_seconds_1', 'firm_seconds_10', 'firm_seconds_11', 'firm_seconds_12', 'firm_seconds_13', 'firm_seconds_14', 'firm_seconds_15', 'firm_seconds_16', 'firm_seconds_17', 'firm_seconds_18', 'firm_seconds_19', 'firm_seconds_2', 'firm_seconds_20', 'firm_seconds_3', 'firm_seconds_4', 'firm_seconds_5', 'firm_seconds_6', 'firm_seconds_7', 'firm_seconds_8', 'firm_seconds_9', 'firm_starts_1', 'firm_starts_10', 'firm_starts_11', 'firm_starts_12', 'firm_starts_13', 'firm_starts_14', 'firm_starts_15', 'firm_starts_16', 'firm_starts_17', 'firm_starts_18', 'firm_starts_19', 'firm_starts_2', 'firm_starts_20', 'firm_starts_3', 'firm_starts_4', 'firm_starts_5', 'firm_starts_6', 'firm_starts_7', 'firm_starts_8', 'firm_starts_9', 'firm_thirds_1', 'firm_thirds_10', 'firm_thirds_11', 'firm_thirds_12', 'firm_thirds_13', 'firm_thirds_14', 'firm_thirds_15', 'firm_thirds_16', 'firm_thirds_17', 'firm_thirds_18', 'firm_thirds_19', 'firm_thirds_2', 'firm_thirds_20', 'firm_thirds_3', 'firm_thirds_4', 'firm_thirds_5', 'firm_thirds_6', 'firm_thirds_7', 'firm_thirds_8', 'firm_thirds_9', 'first_up_firsts_1', 'first_up_firsts_10', 'first_up_firsts_11', 'first_up_firsts_12', 'first_up_firsts_13', 'first_up_firsts_14', 'first_up_firsts_15', 'first_up_firsts_16', 'first_up_firsts_17', 'first_up_firsts_18', 'first_up_firsts_19', 'first_up_firsts_2', 'first_up_firsts_20', 'first_up_firsts_3', 'first_up_firsts_4', 'first_up_firsts_5', 'first_up_firsts_6', 'first_up_firsts_7', 'first_up_firsts_8', 'first_up_firsts_9', 'first_up_seconds_1', 'first_up_seconds_10', 'first_up_seconds_11', 'first_up_seconds_12', 'first_up_seconds_13', 'first_up_seconds_14', 'first_up_seconds_15', 'first_up_seconds_16', 'first_up_seconds_17', 'first_up_seconds_18', 'first_up_seconds_19', 'first_up_seconds_2', 'first_up_seconds_20', 'first_up_seconds_3', 'first_up_seconds_4', 'first_up_seconds_5', 'first_up_seconds_6', 'first_up_seconds_7', 'first_up_seconds_8', 'first_up_seconds_9', 'first_up_starts_1', 'first_up_starts_10', 'first_up_starts_11', 'first_up_starts_12', 'first_up_starts_13', 'first_up_starts_14', 'first_up_starts_15', 'first_up_starts_16', 'first_up_starts_17', 'first_up_starts_18', 'first_up_starts_19', 'first_up_starts_2', 'first_up_starts_20', 'first_up_starts_3', 'first_up_starts_4', 'first_up_starts_5', 'first_up_starts_6', 'first_up_starts_7', 'first_up_starts_8', 'first_up_starts_9', 'first_up_thirds_1', 'first_up_thirds_10', 'first_up_thirds_11', 'first_up_thirds_12', 'first_up_thirds_13', 'first_up_thirds_14', 'first_up_thirds_15', 'first_up_thirds_16', 'first_up_thirds_17', 'first_up_thirds_18', 'first_up_thirds_19', 'first_up_thirds_2', 'first_up_thirds_20', 'first_up_thirds_3', 'first_up_thirds_4', 'first_up_thirds_5', 'first_up_thirds_6', 'first_up_thirds_7', 'first_up_thirds_8', 'first_up_thirds_9', 'firsts_1', 'firsts_10', 'firsts_11', 'firsts_12', 'firsts_13', 'firsts_14', 'firsts_15', 'firsts_16', 'firsts_17', 'firsts_18', 'firsts_19', 'firsts_2', 'firsts_20', 'firsts_3', 'firsts_4', 'firsts_5', 'firsts_6', 'firsts_7', 'firsts_8', 'firsts_9', 'good_firsts_1', 'good_firsts_10', 'good_firsts_11', 'good_firsts_12', 'good_firsts_13', 'good_firsts_14', 'good_firsts_15', 'good_firsts_16', 'good_firsts_17', 'good_firsts_18', 'good_firsts_19', 'good_firsts_2', 'good_firsts_20', 'good_firsts_3', 'good_firsts_4', 'good_firsts_5', 'good_firsts_6', 'good_firsts_7', 'good_firsts_8', 'good_firsts_9', 'good_seconds_1', 'good_seconds_10', 'good_seconds_11', 'good_seconds_12', 'good_seconds_13', 'good_seconds_14', 'good_seconds_15', 'good_seconds_16', 'good_seconds_17', 'good_seconds_18', 'good_seconds_19', 'good_seconds_2', 'good_seconds_20', 'good_seconds_3', 'good_seconds_4', 'good_seconds_5', 'good_seconds_6', 'good_seconds_7', 'good_seconds_8', 'good_seconds_9', 'good_starts_1', 'good_starts_10', 'good_starts_11', 'good_starts_12', 'good_starts_13', 'good_starts_14', 'good_starts_15', 'good_starts_16', 'good_starts_17', 'good_starts_18', 'good_starts_19', 'good_starts_2', 'good_starts_20', 'good_starts_3', 'good_starts_4', 'good_starts_5', 'good_starts_6', 'good_starts_7', 'good_starts_8', 'good_starts_9', 'good_thirds_1', 'good_thirds_10', 'good_thirds_11', 'good_thirds_12', 'good_thirds_13', 'good_thirds_14', 'good_thirds_15', 'good_thirds_16', 'good_thirds_17', 'good_thirds_18', 'good_thirds_19', 'good_thirds_2', 'good_thirds_20', 'good_thirds_3', 'good_thirds_4', 'good_thirds_5', 'good_thirds_6', 'good_thirds_7', 'good_thirds_8', 'good_thirds_9', 'hcp_rating_1', 'hcp_rating_10', 'hcp_rating_11', 'hcp_rating_12', 'hcp_rating_13', 'hcp_rating_14', 'hcp_rating_15', 'hcp_rating_16', 'hcp_rating_17', 'hcp_rating_18', 'hcp_rating_19', 'hcp_rating_2', 'hcp_rating_20', 'hcp_rating_3', 'hcp_rating_4', 'hcp_rating_5', 'hcp_rating_6', 'hcp_rating_7', 'hcp_rating_8', 'hcp_rating_9', 'heavy_firsts_1', 'heavy_firsts_10', 'heavy_firsts_11', 'heavy_firsts_12', 'heavy_firsts_13', 'heavy_firsts_14', 'heavy_firsts_15', 'heavy_firsts_16', 'heavy_firsts_17', 'heavy_firsts_18', 'heavy_firsts_19', 'heavy_firsts_2', 'heavy_firsts_20', 'heavy_firsts_3', 'heavy_firsts_4', 'heavy_firsts_5', 'heavy_firsts_6', 'heavy_firsts_7', 'heavy_firsts_8', 'heavy_firsts_9', 'heavy_seconds_1', 'heavy_seconds_10', 'heavy_seconds_11', 'heavy_seconds_12', 'heavy_seconds_13', 'heavy_seconds_14', 'heavy_seconds_15', 'heavy_seconds_16', 'heavy_seconds_17', 'heavy_seconds_18', 'heavy_seconds_19', 'heavy_seconds_2', 'heavy_seconds_20', 'heavy_seconds_3', 'heavy_seconds_4', 'heavy_seconds_5', 'heavy_seconds_6', 'heavy_seconds_7', 'heavy_seconds_8', 'heavy_seconds_9', 'heavy_starts_1', 'heavy_starts_10', 'heavy_starts_11', 'heavy_starts_12', 'heavy_starts_13', 'heavy_starts_14', 'heavy_starts_15', 'heavy_starts_16', 'heavy_starts_17', 'heavy_starts_18', 'heavy_starts_19', 'heavy_starts_2', 'heavy_starts_20', 'heavy_starts_3', 'heavy_starts_4', 'heavy_starts_5', 'heavy_starts_6', 'heavy_starts_7', 'heavy_starts_8', 'heavy_starts_9', 'heavy_thirds_1', 'heavy_thirds_10', 'heavy_thirds_11', 'heavy_thirds_12', 'heavy_thirds_13', 'heavy_thirds_14', 'heavy_thirds_15', 'heavy_thirds_16', 'heavy_thirds_17', 'heavy_thirds_18', 'heavy_thirds_19', 'heavy_thirds_2', 'heavy_thirds_20', 'heavy_thirds_3', 'heavy_thirds_4', 'heavy_thirds_5', 'heavy_thirds_6', 'heavy_thirds_7', 'heavy_thirds_8', 'heavy_thirds_9', 'horse_name_1', 'horse_name_10', 'horse_name_11', 'horse_name_12', 'horse_name_13', 'horse_name_14', 'horse_name_15', 'horse_name_16', 'horse_name_17', 'horse_name_18', 'horse_name_19', 'horse_name_2', 'horse_name_20', 'horse_name_3', 'horse_name_4', 'horse_name_5', 'horse_name_6', 'horse_name_7', 'horse_name_8', 'horse_name_9', 'jockey_1', 'jockey_10', 'jockey_11', 'jockey_12', 'jockey_13', 'jockey_14', 'jockey_15', 'jockey_16', 'jockey_17', 'jockey_18', 'jockey_19', 'jockey_2', 'jockey_20', 'jockey_3', 'jockey_4', 'jockey_5', 'jockey_6', 'jockey_7', 'jockey_8', 'jockey_9', 'margin_1', 'margin_10', 'margin_11', 'margin_12', 'margin_13', 'margin_14', 'margin_15', 'margin_16', 'margin_17', 'margin_18', 'margin_19', 'margin_2', 'margin_20', 'margin_3', 'margin_4', 'margin_5', 'margin_6', 'margin_7', 'margin_8', 'margin_9', 'number_1', 'number_10', 'number_11', 'number_12', 'number_13', 'number_14', 'number_15', 'number_16', 'number_17', 'number_18', 'number_19', 'number_2', 'number_20', 'number_3', 'number_4', 'number_5', 'number_6', 'number_7', 'number_8', 'number_9', 'prizemoney_1', 'prizemoney_10', 'prizemoney_11', 'prizemoney_12', 'prizemoney_13', 'prizemoney_14', 'prizemoney_15', 'prizemoney_16', 'prizemoney_17', 'prizemoney_18', 'prizemoney_19', 'prizemoney_2', 'prizemoney_20', 'prizemoney_3', 'prizemoney_4', 'prizemoney_5', 'prizemoney_6', 'prizemoney_7', 'prizemoney_8', 'prizemoney_9', 'second_up_firsts_1', 'second_up_firsts_10', 'second_up_firsts_11', 'second_up_firsts_12', 'second_up_firsts_13', 'second_up_firsts_14', 'second_up_firsts_15', 'second_up_firsts_16', 'second_up_firsts_17', 'second_up_firsts_18', 'second_up_firsts_19', 'second_up_firsts_2', 'second_up_firsts_20', 'second_up_firsts_3', 'second_up_firsts_4', 'second_up_firsts_5', 'second_up_firsts_6', 'second_up_firsts_7', 'second_up_firsts_8', 'second_up_firsts_9', 'second_up_seconds_1', 'second_up_seconds_10', 'second_up_seconds_11', 'second_up_seconds_12', 'second_up_seconds_13', 'second_up_seconds_14', 'second_up_seconds_15', 'second_up_seconds_16', 'second_up_seconds_17', 'second_up_seconds_18', 'second_up_seconds_19', 'second_up_seconds_2', 'second_up_seconds_20', 'second_up_seconds_3', 'second_up_seconds_4', 'second_up_seconds_5', 'second_up_seconds_6', 'second_up_seconds_7', 'second_up_seconds_8', 'second_up_seconds_9', 'second_up_starts_1', 'second_up_starts_10', 'second_up_starts_11', 'second_up_starts_12', 'second_up_starts_13', 'second_up_starts_14', 'second_up_starts_15', 'second_up_starts_16', 'second_up_starts_17', 'second_up_starts_18', 'second_up_starts_19', 'second_up_starts_2', 'second_up_starts_20', 'second_up_starts_3', 'second_up_starts_4', 'second_up_starts_5', 'second_up_starts_6', 'second_up_starts_7', 'second_up_starts_8', 'second_up_starts_9', 'second_up_thirds_1', 'second_up_thirds_10', 'second_up_thirds_11', 'second_up_thirds_12', 'second_up_thirds_13', 'second_up_thirds_14', 'second_up_thirds_15', 'second_up_thirds_16', 'second_up_thirds_17', 'second_up_thirds_18', 'second_up_thirds_19', 'second_up_thirds_2', 'second_up_thirds_20', 'second_up_thirds_3', 'second_up_thirds_4', 'second_up_thirds_5', 'second_up_thirds_6', 'second_up_thirds_7', 'second_up_thirds_8', 'second_up_thirds_9', 'seconds_1', 'seconds_10', 'seconds_11', 'seconds_12', 'seconds_13', 'seconds_14', 'seconds_15', 'seconds_16', 'seconds_17', 'seconds_18', 'seconds_19', 'seconds_2', 'seconds_20', 'seconds_3', 'seconds_4', 'seconds_5', 'seconds_6', 'seconds_7', 'seconds_8', 'seconds_9', 'soft_firsts_1', 'soft_firsts_10', 'soft_firsts_11', 'soft_firsts_12', 'soft_firsts_13', 'soft_firsts_14', 'soft_firsts_15', 'soft_firsts_16', 'soft_firsts_17', 'soft_firsts_18', 'soft_firsts_19', 'soft_firsts_2', 'soft_firsts_20', 'soft_firsts_3', 'soft_firsts_4', 'soft_firsts_5', 'soft_firsts_6', 'soft_firsts_7', 'soft_firsts_8', 'soft_firsts_9', 'soft_seconds_1', 'soft_seconds_10', 'soft_seconds_11', 'soft_seconds_12', 'soft_seconds_13', 'soft_seconds_14', 'soft_seconds_15', 'soft_seconds_16', 'soft_seconds_17', 'soft_seconds_18', 'soft_seconds_19', 'soft_seconds_2', 'soft_seconds_20', 'soft_seconds_3', 'soft_seconds_4', 'soft_seconds_5', 'soft_seconds_6', 'soft_seconds_7', 'soft_seconds_8', 'soft_seconds_9', 'soft_starts_1', 'soft_starts_10', 'soft_starts_11', 'soft_starts_12', 'soft_starts_13', 'soft_starts_14', 'soft_starts_15', 'soft_starts_16', 'soft_starts_17', 'soft_starts_18', 'soft_starts_19', 'soft_starts_2', 'soft_starts_20', 'soft_starts_3', 'soft_starts_4', 'soft_starts_5', 'soft_starts_6', 'soft_starts_7', 'soft_starts_8', 'soft_starts_9', 'soft_thirds_1', 'soft_thirds_10', 'soft_thirds_11', 'soft_thirds_12', 'soft_thirds_13', 'soft_thirds_14', 'soft_thirds_15', 'soft_thirds_16', 'soft_thirds_17', 'soft_thirds_18', 'soft_thirds_19', 'soft_thirds_2', 'soft_thirds_20', 'soft_thirds_3', 'soft_thirds_4', 'soft_thirds_5', 'soft_thirds_6', 'soft_thirds_7', 'soft_thirds_8', 'soft_thirds_9', 'starting_price_1', 'starting_price_10', 'starting_price_11', 'starting_price_12', 'starting_price_13', 'starting_price_14', 'starting_price_15', 'starting_price_16', 'starting_price_17', 'starting_price_18', 'starting_price_19', 'starting_price_2', 'starting_price_20', 'starting_price_3', 'starting_price_4', 'starting_price_5', 'starting_price_6', 'starting_price_7', 'starting_price_8', 'starting_price_9', 'starts_1', 'starts_10', 'starts_11', 'starts_12', 'starts_13', 'starts_14', 'starts_15', 'starts_16', 'starts_17', 'starts_18', 'starts_19', 'starts_2', 'starts_20', 'starts_3', 'starts_4', 'starts_5', 'starts_6', 'starts_7', 'starts_8', 'starts_9', 'synthetic_firsts_1', 'synthetic_firsts_10', 'synthetic_firsts_11', 'synthetic_firsts_12', 'synthetic_firsts_13', 'synthetic_firsts_14', 'synthetic_firsts_15', 'synthetic_firsts_16', 'synthetic_firsts_17', 'synthetic_firsts_18', 'synthetic_firsts_19', 'synthetic_firsts_2', 'synthetic_firsts_20', 'synthetic_firsts_3', 'synthetic_firsts_4', 'synthetic_firsts_5', 'synthetic_firsts_6', 'synthetic_firsts_7', 'synthetic_firsts_8', 'synthetic_firsts_9', 'synthetic_seconds_1', 'synthetic_seconds_10', 'synthetic_seconds_11', 'synthetic_seconds_12', 'synthetic_seconds_13', 'synthetic_seconds_14', 'synthetic_seconds_15', 'synthetic_seconds_16', 'synthetic_seconds_17', 'synthetic_seconds_18', 'synthetic_seconds_19', 'synthetic_seconds_2', 'synthetic_seconds_20', 'synthetic_seconds_3', 'synthetic_seconds_4', 'synthetic_seconds_5', 'synthetic_seconds_6', 'synthetic_seconds_7', 'synthetic_seconds_8', 'synthetic_seconds_9', 'synthetic_starts_1', 'synthetic_starts_10', 'synthetic_starts_11', 'synthetic_starts_12', 'synthetic_starts_13', 'synthetic_starts_14', 'synthetic_starts_15', 'synthetic_starts_16', 'synthetic_starts_17', 'synthetic_starts_18', 'synthetic_starts_19', 'synthetic_starts_2', 'synthetic_starts_20', 'synthetic_starts_3', 'synthetic_starts_4', 'synthetic_starts_5', 'synthetic_starts_6', 'synthetic_starts_7', 'synthetic_starts_8', 'synthetic_starts_9', 'synthetic_thirds_1', 'synthetic_thirds_10', 'synthetic_thirds_11', 'synthetic_thirds_12', 'synthetic_thirds_13', 'synthetic_thirds_14', 'synthetic_thirds_15', 'synthetic_thirds_16', 'synthetic_thirds_17', 'synthetic_thirds_18', 'synthetic_thirds_19', 'synthetic_thirds_2', 'synthetic_thirds_20', 'synthetic_thirds_3', 'synthetic_thirds_4', 'synthetic_thirds_5', 'synthetic_thirds_6', 'synthetic_thirds_7', 'synthetic_thirds_8', 'synthetic_thirds_9', 'thirds_1', 'thirds_10', 'thirds_11', 'thirds_12', 'thirds_13', 'thirds_14', 'thirds_15', 'thirds_16', 'thirds_17', 'thirds_18', 'thirds_19', 'thirds_2', 'thirds_20', 'thirds_3', 'thirds_4', 'thirds_5', 'thirds_6', 'thirds_7', 'thirds_8', 'thirds_9', 'track_dist_firsts_1', 'track_dist_firsts_10', 'track_dist_firsts_11', 'track_dist_firsts_12', 'track_dist_firsts_13', 'track_dist_firsts_14', 'track_dist_firsts_15', 'track_dist_firsts_16', 'track_dist_firsts_17', 'track_dist_firsts_18', 'track_dist_firsts_19', 'track_dist_firsts_2', 'track_dist_firsts_20', 'track_dist_firsts_3', 'track_dist_firsts_4', 'track_dist_firsts_5', 'track_dist_firsts_6', 'track_dist_firsts_7', 'track_dist_firsts_8', 'track_dist_firsts_9', 'track_dist_seconds_1', 'track_dist_seconds_10', 'track_dist_seconds_11', 'track_dist_seconds_12', 'track_dist_seconds_13', 'track_dist_seconds_14', 'track_dist_seconds_15', 'track_dist_seconds_16', 'track_dist_seconds_17', 'track_dist_seconds_18', 'track_dist_seconds_19', 'track_dist_seconds_2', 'track_dist_seconds_20', 'track_dist_seconds_3', 'track_dist_seconds_4', 'track_dist_seconds_5', 'track_dist_seconds_6', 'track_dist_seconds_7', 'track_dist_seconds_8', 'track_dist_seconds_9', 'track_dist_starts_1', 'track_dist_starts_10', 'track_dist_starts_11', 'track_dist_starts_12', 'track_dist_starts_13', 'track_dist_starts_14', 'track_dist_starts_15', 'track_dist_starts_16', 'track_dist_starts_17', 'track_dist_starts_18', 'track_dist_starts_19', 'track_dist_starts_2', 'track_dist_starts_20', 'track_dist_starts_3', 'track_dist_starts_4', 'track_dist_starts_5', 'track_dist_starts_6', 'track_dist_starts_7', 'track_dist_starts_8', 'track_dist_starts_9', 'track_dist_thirds_1', 'track_dist_thirds_10', 'track_dist_thirds_11', 'track_dist_thirds_12', 'track_dist_thirds_13', 'track_dist_thirds_14', 'track_dist_thirds_15', 'track_dist_thirds_16', 'track_dist_thirds_17', 'track_dist_thirds_18', 'track_dist_thirds_19', 'track_dist_thirds_2', 'track_dist_thirds_20', 'track_dist_thirds_3', 'track_dist_thirds_4', 'track_dist_thirds_5', 'track_dist_thirds_6', 'track_dist_thirds_7', 'track_dist_thirds_8', 'track_dist_thirds_9', 'track_firsts_1', 'track_firsts_10', 'track_firsts_11', 'track_firsts_12', 'track_firsts_13', 'track_firsts_14', 'track_firsts_15', 'track_firsts_16', 'track_firsts_17', 'track_firsts_18', 'track_firsts_19', 'track_firsts_2', 'track_firsts_20', 'track_firsts_3', 'track_firsts_4', 'track_firsts_5', 'track_firsts_6', 'track_firsts_7', 'track_firsts_8', 'track_firsts_9', 'track_location', 'track_name', 'track_seconds_1', 'track_seconds_10', 'track_seconds_11', 'track_seconds_12', 'track_seconds_13', 'track_seconds_14', 'track_seconds_15', 'track_seconds_16', 'track_seconds_17', 'track_seconds_18', 'track_seconds_19', 'track_seconds_2', 'track_seconds_20', 'track_seconds_3', 'track_seconds_4', 'track_seconds_5', 'track_seconds_6', 'track_seconds_7', 'track_seconds_8', 'track_seconds_9', 'track_starts_1', 'track_starts_10', 'track_starts_11', 'track_starts_12', 'track_starts_13', 'track_starts_14', 'track_starts_15', 'track_starts_16', 'track_starts_17', 'track_starts_18', 'track_starts_19', 'track_starts_2', 'track_starts_20', 'track_starts_3', 'track_starts_4', 'track_starts_5', 'track_starts_6', 'track_starts_7', 'track_starts_8', 'track_starts_9', 'track_thirds_1', 'track_thirds_10', 'track_thirds_11', 'track_thirds_12', 'track_thirds_13', 'track_thirds_14', 'track_thirds_15', 'track_thirds_16', 'track_thirds_17', 'track_thirds_18', 'track_thirds_19', 'track_thirds_2', 'track_thirds_20', 'track_thirds_3', 'track_thirds_4', 'track_thirds_5', 'track_thirds_6', 'track_thirds_7', 'track_thirds_8', 'track_thirds_9', 'track_type', 'trainer_1', 'trainer_10', 'trainer_11', 'trainer_12', 'trainer_13', 'trainer_14', 'trainer_15', 'trainer_16', 'trainer_17', 'trainer_18', 'trainer_19', 'trainer_2', 'trainer_20', 'trainer_3', 'trainer_4', 'trainer_5', 'trainer_6', 'trainer_7', 'trainer_8', 'trainer_9', 'weight_1', 'weight_10', 'weight_11', 'weight_12', 'weight_13', 'weight_14', 'weight_15', 'weight_16', 'weight_17', 'weight_18', 'weight_19', 'weight_2', 'weight_20', 'weight_3', 'weight_4', 'weight_5', 'weight_6', 'weight_7', 'weight_8', 'weight_9']

MAX_HORSES = 20

the_list = []

def main():
    global the_list

    results_urls = get_prediction_urls()

    for url in results_urls:
        print("Checking URL: " + url)
        get_results(url)
        time.sleep(1.5)
    print("Completed Successfully")

def get_results(url):
    the_list = []

    all_form_url = url
    all_form_page = requests.get(all_form_url)
    all_form_page_tree = html.fromstring(all_form_page.content)

    race_count = all_form_page_tree.xpath(clean_xpath(RACE_COUNT_XPATH))
    print("Race count: " + str(len(race_count) - 1))

    for current_race in range(len(race_count) - 1):
        current_race = current_race + 1
        print("Processing places race [{race_number}]..".format(race_number=current_race))
        temp_obj = {}
        index = 1

        ###
        # Get Track info
        ###
        track_loction = all_form_page_tree.xpath(clean_xpath(TRACK_LOCATION_XPATH))
        if(len(track_loction) > 0):
            normalized_track_loction = unicodedata.normalize("NFKD", track_loction[0].text_content())
            # print(normalized_track_loction)
            temp_obj['track_location'] = normalized_track_loction.split(':')[0].strip()
            temp_obj['date'] = normalized_track_loction.split('\n')[2].replace('\n', '').replace('\r', '').strip()

        track_details = all_form_page_tree.xpath(clean_xpath(TRACK_DETAILS_XPATH.format(race_number=current_race)))
        if(len(track_details) > 0):
            normalized_track_details = unicodedata.normalize("NFKD", track_details[0].text_content())
            # print(normalized_track_details)
            # print(re.search("(?:Track Type: +)(\w+)(?: +Field)", normalized_track_details).group(1))
            if(re.search('(?:Track Type: +)(\w+)(?: +Field)', normalized_track_details) != None):
                temp_obj['track_type'] = re.search('(?:Track Type: +)(\w+)(?: +Field)', normalized_track_details).group(1)
            if(re.search('(?:Track Name: +)(\w+)(?: +Track Type)', normalized_track_details) != None):
                temp_obj['track_name'] = re.search('(?:Track Name: +)(\w+)(?: +Track Type)', normalized_track_details).group(1)

        ###
        # Process results page
        # Gets horse number and winner, etc.
        ###
        while(index <= MAX_HORSES):
            # for feature in RACE_FEATURES:
            xpath = ''
            xpath = ALL_FORM_RACE_FEATURE_XPATHS['number'].format(race_number=current_race, number=index)
            xpath = clean_xpath(xpath)
            res = all_form_page_tree.xpath(xpath)[0].text_content() if len(all_form_page_tree.xpath(xpath)) > 0 else None
            print('Index[{}] Processing feature[{}] result[{}] with xpath: {}'.format(index, 'number', res, xpath))
            temp_obj['{feature}_{index}'.format(feature='number', index=index)] = res


            index = index + 1
            try:
                all_form_page_tree.xpath(clean_xpath(str(ALL_FORM_RACE_FEATURE_XPATHS['number'].format(race_number=current_race, number=index))))[0]
            except Exception as e:
                print("No more horses in race results")
                print(e)
                break

        ###
        # Process all form page
        # Get most of the stats for the horse.
        ###
        # print(list(temp_obj))
        # pprint.pprint(temp_obj)
        # input()
        for key in list(temp_obj):
            if key.find('number_') != -1:

                existing_index = key.split('_')[1]
                existing_horse_number = temp_obj[key]
                index = 1
                print('Existing index: ', existing_index)

                if existing_horse_number is None:
                    continue

                while(True):
                    ###
                    # Find pre-existing horse number index by index
                    ###
                    xpath = ALL_FORM_RACE_FEATURE_XPATHS['number'].format(race_number=current_race, number=index)
                    xpath = clean_xpath(xpath)
                    # print(xpath)
                    current_horse_number = all_form_page_tree.xpath(xpath)[0].text_content()

                    print('Existing horse number[{}] Current horse number[{}]'.format(existing_horse_number, current_horse_number))
                    if existing_horse_number == current_horse_number:
                        print('Index[{}] Scraping all form for horse numbered[{}]'.format(index, current_horse_number))
                        # Scrape the data for the horse
                        for feature in ALL_FORM_RACE_FEATURES:
                            xpath = ''
                            xpath = ALL_FORM_RACE_FEATURE_XPATHS[feature].format(race_number=current_race, number=index)
                            xpath = clean_xpath(xpath)
                            res = all_form_page_tree.xpath(xpath)[0].text_content() if len(all_form_page_tree.xpath(xpath)) > 0 else None
                            print('Processing feature[{}] result[{}] with xpath: {}'.format(feature, res.encode('utf-8'), xpath))
                            temp_obj['{feature}_{index}'.format(feature=feature, index=existing_index)] = res
                            # input()

                        print("Scraping stats for horse at index[{}] named[{}]".format(index, temp_obj['horse_name_'+str(existing_index)].encode('utf-8')))
                        # Get per horse info
                        array_of_horse_infos = all_form_page_tree.xpath(clean_xpath(ALL_FORM_ALL_HORSES_XPATH))
                        for horse_info in array_of_horse_infos:
                            # print("Horse info", horse_info.text_content())
                            normalized = unicodedata.normalize("NFKD", horse_info.text_content())
                            cleaned = [x.strip() for x in normalized.strip().replace('\t', '').replace('\r', '').split('\n') if x]
                            # print(cleaned)
                            if cleaned[1].find(temp_obj['horse_name_{index}'.format(index=existing_index)]) != -1:
                                for element in cleaned:
                                    if element.find(' year old') != -1:
                                        temp_obj['age_{index}'.format(index=existing_index)] = re.findall('\d+', element)[0]
                                    if element.find('Prizemoney') != -1:
                                        temp_obj['prizemoney_{index}'.format(index=existing_index)] = re.findall('\d+,?\d*,?\d*,?\d*', element)[0].replace(',', '')
                                    if element.find('Record:') != -1:
                                        temp_obj['starts_{index}'.format(index=existing_index)] = re.findall('\d+', element)[0]
                                        temp_obj['firsts_{index}'.format(index=existing_index)] = re.findall('\d+', element)[1]
                                        temp_obj['seconds_{index}'.format(index=existing_index)] = re.findall('\d+', element)[2]
                                        temp_obj['thirds_{index}'.format(index=existing_index)] = re.findall('\d+', element)[3]
                                    if element.find('1st Up:') != -1:
                                        temp_obj['first_up_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('1st Up: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['first_up_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('1st Up: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['first_up_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('1st Up: \d+:\d+-\d+-\d+', element)[0])[3]
                                        temp_obj['first_up_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('1st Up: \d+:\d+-\d+-\d+', element)[0])[4]
                                    if element.find('2nd Up:') != -1:
                                        temp_obj['second_up_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('2nd Up: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['second_up_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('2nd Up: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['second_up_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('2nd Up: \d+:\d+-\d+-\d+', element)[0])[3]
                                        temp_obj['second_up_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('2nd Up: \d+:\d+-\d+-\d+', element)[0])[4]
                                    if element.find('Track:') != -1:
                                        temp_obj['track_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['track_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['track_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['track_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Dist:') != -1:
                                        temp_obj['dist_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Dist: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['dist_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Dist: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['dist_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Dist: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['dist_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Dist: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Track/Dist:') != -1:
                                        temp_obj['track_dist_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track/Dist: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['track_dist_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track/Dist: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['track_dist_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track/Dist: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['track_dist_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Track/Dist: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Firm:') != -1:
                                        temp_obj['firm_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Firm: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['firm_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Firm: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['firm_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Firm: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['firm_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Firm: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Good:') != -1:
                                        temp_obj['good_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Good: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['good_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Good: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['good_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Good: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['good_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Good: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Soft:') != -1:
                                        temp_obj['soft_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Soft: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['soft_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Soft: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['soft_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Soft: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['soft_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Soft: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Heavy:') != -1:
                                        temp_obj['heavy_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Heavy: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['heavy_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Heavy: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['heavy_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Heavy: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['heavy_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Heavy: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Synthetic:') != -1:
                                        temp_obj['synthetic_starts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Synthetic: \d+:\d+-\d+-\d+', element)[0])[0]
                                        temp_obj['synthetic_firsts_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Synthetic: \d+:\d+-\d+-\d+', element)[0])[1]
                                        temp_obj['synthetic_seconds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Synthetic: \d+:\d+-\d+-\d+', element)[0])[2]
                                        temp_obj['synthetic_thirds_{index}'.format(index=existing_index)] = re.findall('\d+', re.findall('Synthetic: \d+:\d+-\d+-\d+', element)[0])[3]
                                    if element.find('Breeder:') != -1:
                                        temp_obj['breeder_{index}'.format(index=existing_index)] = element.split(':', maxsplit=1)[1]
                                    if element.find('Owners:') != -1:
                                        temp_obj['breeder_{index}'.format(index=existing_index)] = element.split(':', maxsplit=1)[1]
                        break
                    index = index + 1
                    try:
                        all_form_page_tree.xpath(clean_xpath(str(ALL_FORM_RACE_FEATURE_XPATHS['number'].format(race_number=current_race, number=index))))[0]
                    except Exception as e:
                        print("No more horses in race all form")
                        print(e)
                        break
        # print(temp_obj)
        # input()
        for header in HEADERS:
            if header not in temp_obj.keys():
                temp_obj[header] = 0
        the_list.append(temp_obj)

    file_name = 'out_future_scraped/' + str(url.split('Key=')[1].split('&')[0])
    pd.read_json(json.dumps(the_list)).to_csv(file_name + '.csv', index=False)
    with open(file_name + '.json', 'w') as out:
        out.write(json.dumps(the_list))

def get_prediction_urls():
    results = []

    home = requests.get(BASE_URL)
    home_tree = html.fromstring(home.content)

    # print(home_tree.xpath(clean_xpath(GET_TOMORROW_URLS_XPATH)))

    for path in home_tree.xpath(clean_xpath(GET_TOMORROW_URLS_XPATH)):
        if path.get('href') is not None and path.get('href').find('Results') != -1:
            results.append(str(BASE_URL + str(path.get('href').replace('Results','AllForm') + ALL_FORM_ENDING)))
        elif path.get('href') is not None:
            results.append(str(BASE_URL + str(path.get('href').replace('Form','AllForm') + ALL_FORM_ENDING)))


    print(results)
    return results

def clean_xpath(xpath):
    return xpath.replace("tbody", '')

main()
