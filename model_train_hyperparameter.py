# https://github.com/awslabs/amazon-sagemaker-examples/blob/master/hyperparameter_tuning/xgboost_direct_marketing/hpo_xgboost_direct_marketing_sagemaker_python_sdk.ipynb
import sagemaker
import boto3
from sagemaker.tuner import IntegerParameter, CategoricalParameter, ContinuousParameter, HyperparameterTuner

import numpy as np                                # For matrix operations and numerical processing
import pandas as pd                               # For munging tabular data
import os
from sagemaker.amazon.amazon_estimator import get_image_uri
import random

FILE_DATA = 'data/data_raw_formatted_with_zeros.csv'
region = 'ap-southeast-2'
bucket='tullys-models' # put your s3 bucket name here, and create s3 bucket
s3_prefix = 'sagemaker/xgboost-horseracing-hyperparameter'

def write_to_s3(fobj, bucket, key):
    return boto3.Session().resource('s3').Bucket(bucket).Object(key).upload_fileobj(fobj)

def upload_to_s3(bucket, channel, filename):
    fobj=open(filename, 'rb')
    key = s3_prefix+'/'+channel
    url = 's3://{}/{}/{}'.format(bucket, key, filename)
    print('Writing to {}'.format(url))
    write_to_s3(fobj, bucket, key)

def data_split(FILE_DATA, FILE_TRAIN, FILE_VALIDATION, FILE_TEST, PERCENT_TRAIN, PERCENT_VALIDATION, PERCENT_TEST):
    data = [l for l in open(FILE_DATA, 'r')]
    train_file = open(FILE_TRAIN, 'w')
    valid_file = open(FILE_VALIDATION, 'w')
    tests_file = open(FILE_TEST, 'w')

    num_of_data = len(data)
    num_train = int((PERCENT_TRAIN/100.0)*num_of_data)
    num_tests = int((PERCENT_TEST/100.0)*num_of_data)
    num_valid = int((PERCENT_VALIDATION/100.0)*num_of_data)

    data_fractions = [num_train, num_tests, num_valid]
    split_data = [[],[],[]]

    rand_data_ind = 0

    for split_ind, fraction in enumerate(data_fractions):
        for i in range(fraction):
            rand_data_ind = random.randint(0, len(data)-1)
            split_data[split_ind].append(data[rand_data_ind])
            data.pop(rand_data_ind)

    for l in split_data[0]:
        train_file.write(l)

    for l in split_data[1]:
        valid_file.write(l)

    for l in split_data[2]:
        tests_file.write(l)

    train_file.close()
    valid_file.close()
    tests_file.close()

#split the downloaded data into train/test/validation files
FILE_TRAIN = FILE_DATA + '.train.csv'
FILE_VALIDATION = FILE_DATA + '.validation.csv'
FILE_TEST = FILE_DATA + '.test.csv'
PERCENT_TRAIN = 70
PERCENT_VALIDATION = 15
PERCENT_TEST = 15
data_split(FILE_DATA, FILE_TRAIN, FILE_VALIDATION, FILE_TEST, PERCENT_TRAIN, PERCENT_VALIDATION, PERCENT_TEST)


#upload the files to the S3 bucket
# upload_to_s3(bucket, 'train', FILE_TRAIN)
# upload_to_s3(bucket, 'test', FILE_TEST)
# upload_to_s3(bucket, 'validation', FILE_VALIDATION)

boto3.setup_default_session(profile_name='tully', region_name=region)
client = boto3.client('sagemaker')
# sess = sagemaker.Session()

container = get_image_uri(region, 'xgboost')
role = 'arn:aws:iam::928290690802:role/service-role/AmazonSageMaker-ExecutionRole-20180413T170070'

xgb = sagemaker.estimator.Estimator(
    container,
    role,
    train_instance_count=1,
    train_instance_type='ml.m5.large',
    output_path='s3://{}/{}/output'.format(bucket, s3_prefix)
    # sagemaker_session=sess
)

with open('max_indexes.txt', mode='r', encoding='utf-8') as f:
    num_classes = f.read()

xgb.set_hyperparameters(
    objective="multi:softprob",
    num_class=str(num_classes),
    eval_metric="mlogloss"
)

# xgb.set_hyperparameters(
#     objective="multi:softprob",
#     num_class=str(num_classes),
#     eval_metric="merror",
#     subsample="0.7"
# )

# merror
# mlogloss

objective_metric_name = 'validation:mlogloss'

# https://docs.aws.amazon.com/sagemaker/latest/dg/xgboost-tuning.html
hyperparameter_ranges = {
    # 'alpha': ContinuousParameter(0, 1000),
    'colsample_bylevel': ContinuousParameter(0.1, 1),
    'colsample_bytree': ContinuousParameter(0.5, 1),
    'eta': ContinuousParameter(0.1, 0.5),
    'gamma': ContinuousParameter(0, 5),
    'lambda': ContinuousParameter(1, 1000),
    'max_delta_step': IntegerParameter(0, 10),
    'max_depth': IntegerParameter(1, 10),
    'min_child_weight': ContinuousParameter(0, 120),
    'num_round': IntegerParameter(1, 4000),
    'subsample': ContinuousParameter(0.5, 1)
}

# hyperparameter_ranges = {
#     'max_depth': IntegerParameter(1, 10),
#     'num_round': IntegerParameter(100, 110)
# }


tuner = HyperparameterTuner(
    estimator=xgb,
    objective_metric_name=objective_metric_name,
    hyperparameter_ranges=hyperparameter_ranges,
    max_jobs=40,
    max_parallel_jobs=1,
    objective_type='Minimize'
)

s3_input_train = sagemaker.s3_input(s3_data='s3://{}/{}/train'.format(bucket, s3_prefix), content_type='csv')
s3_input_validation = sagemaker.s3_input(s3_data='s3://{}/{}/validation'.format(bucket, s3_prefix), content_type='csv')

tuner.fit({'train': s3_input_train, 'validation': s3_input_validation}, include_cls_metadata=False)

client.describe_hyper_parameter_tuning_job(HyperParameterTuningJobName=tuner.latest_tuning_job.job_name)['HyperParameterTuningJobStatus']
