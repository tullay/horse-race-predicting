import boto3
from time import gmtime, strftime
from sagemaker.amazon.amazon_estimator import get_image_uri
import time
import math
import numpy
import csv
from random import randint
import pprint
import warnings
import json
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.externals import joblib
import re
import sys
import os
from datetime import datetime, timedelta
import pickle
import encoder_helper
import yaml

warnings.simplefilter('ignore', DeprecationWarning)

region = 'ap-southeast-2'

PREDICTION_OUT_FILE_LOCATION = 'out_predictions/'

CSV_FILE_WITH_HEADERS_LOCATION= 'out_future/'
CSV_FILE_WITH_HEADERS_POSTFIX = '_original_with_headers.csv'
CSV_FILES_LOCATION = 'out_future_formatted'

with open('config.yaml', 'r') as file:
    config = yaml.load(file)
model_id = config['model_id']

# model_id = '2018-10-06'
# model_id = 'horseracing-xgboost-regression-2018-10-06-03-02-18'

def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)


if os.path.exists(os.path.join(os.path.expanduser('~'), '.aws')):
    boto3.setup_default_session(profile_name='tully')

client = boto3.client('sagemaker', region_name=region)

container = get_image_uri(region, 'xgboost')

resp = client.describe_endpoint(EndpointName=model_id)
status = resp['EndpointStatus']
print("Status: " + status)

while status=='Creating':
    time.sleep(60)
    resp = client.describe_endpoint(EndpointName=model_id)
    status = resp['EndpointStatus']
    print("Status: " + status)

print("Arn: " + resp['EndpointArn'])
print("Status: " + status)

runtime_client = boto3.client('runtime.sagemaker', region_name='ap-southeast-2')

# head -1 out/my_csv_winner_with_zeros.csv.validation > out/my_csv_winner_with_zeros.csv.validation.single

import json
from itertools import islice
import math
import struct
import csv


for filename in os.listdir(CSV_FILES_LOCATION):
    if filename.endswith(".csv"):
        print('Predicting filename[{}]'.format(CSV_FILES_LOCATION+'/'+filename))
        # row_reader = csv.reader(open(CSV_FILES_LOCATION+'/'+filename, "r"))
        # continue
        df = pd.read_csv(CSV_FILE_WITH_HEADERS_LOCATION + filename.split('_cleaned_with_zeros.csv')[0] + CSV_FILE_WITH_HEADERS_POSTFIX)
        headers_list = df.columns.values.tolist()

        df_formatted = pd.read_csv(CSV_FILES_LOCATION+'/'+filename, header=None)

        columns = ['track_location_original', 'race_number', 'winning_horse_number', 'winning_confidence', 'horse_name_original']

        df_results = pd.DataFrame(columns=columns)

        total = 0
        for row_idx, row in df_formatted.iterrows():
            # print(row.tolist())
            # print(df.iloc[int(row_idx)].tolist())
            # input()
            temp_row_dict = {}

            print('Getting results for row[{}] length[{}]'.format(row_idx, len(row)))
            payload = str(row.tolist()).replace('[', '').replace(']', '')


            response = runtime_client.invoke_endpoint(EndpointName=model_id,
                                               ContentType='text/csv',
                                               Body=payload)
            result = response['Body'].read()
            result = result.decode("utf-8")
            # print(result)
            winning_class = 0
            winning_confidence = 0
            result_dict = str(result).replace('[', '').replace(']', '').replace(',', '').split(' ')
            # print(result_dict)
            for res_idx, res in enumerate(result_dict):
                if float("{:.8f}".format(float(res))) > float(winning_confidence):
                    winning_confidence = res
                    winning_class = res_idx

            print('Winning class[{}]'.format(winning_class))
            ###
            # Decode Winner features
            ###
            label_encoder = encoder_helper.load_label_encoder('encoders/' + model_id + '/winning_number.pickle')

            winning_number = float(label_encoder.inverse_transform(int(winning_class)))

            temp_row_dict['race_number'] = row_idx+1
            temp_row_dict['winning_horse_number'] = winning_number
            temp_row_dict['winning_confidence'] = winning_confidence

            # features_to_decode = ['horse_name_', 'track_location', 'track_name', 'breeder_', 'jockey_', 'trainer_', 'track_type']
            features_to_decode = ['horse_name_', 'track_location']
            for col_idx, value in enumerate(df.columns.values.tolist()):
                for feature in features_to_decode:
                    encoder_path = 'encoders/' + model_id + '/' + feature + '.pickle'
                    if(str(value).find(feature) != -1):
                        # print(winning_number, re.findall('\d+', value)[0], hasNumbers(value), int(re.findall('\d+', value)[0]) == winning_number)
                        if hasNumbers(value) and int(re.findall('\d+', value)[0]) == winning_number:
                            print(feature)
                            label_encoder = encoder_helper.load_label_encoder(encoder_path)
                            decoded_value = label_encoder.inverse_transform(int(row[col_idx]))
                            original_value = df.iloc[row_idx].loc[feature+str(re.findall('\d+', value)[0])]
                            temp_row_dict[feature+'decoded'] = decoded_value
                            temp_row_dict[feature+'original'] = original_value
                            # print(str('\t\tFeature[{}] Original[{}] Decoded[{}]'.format(feature, original_value, decoded_value)))
                        elif not hasNumbers(value):
                            label_encoder = encoder_helper.load_label_encoder(encoder_path)
                            decoded_value = label_encoder.inverse_transform(int(row[col_idx]))
                            original_value = df.iloc[row_idx].loc[feature]
                            temp_row_dict[feature+'_decoded'] = decoded_value
                            temp_row_dict[feature+'_original'] = original_value
                            # print(str('\t\tFeature[{}] Original[{}] Decoded[{}]'.format(feature, original_value, decoded_value)))


            # print(string)
            # print(temp_row_dict)
            df_results = df_results.append(temp_row_dict, ignore_index=True)
            # input()
        print(df_results)
        df_results.to_csv(PREDICTION_OUT_FILE_LOCATION+filename, index=False)

frame = pd.DataFrame()
list_ = []
for filename in os.listdir(PREDICTION_OUT_FILE_LOCATION):
    if filename.endswith(".csv"):
        df = pd.read_csv(PREDICTION_OUT_FILE_LOCATION+filename,index_col=None,)
        list_.append(df)
frame = pd.concat(list_)
frame.to_csv('predictions/all_predictions_'+datetime.strftime(datetime.now() + timedelta(days=1), '%Y-%m-%d')+'.csv', index=False)

print('Completed')
