#!/bin/bash
python3.6 -m pip install boto3==1.9.29 lxml==4.2.5 sagemaker==1.11.3 pandas==0.23.4 scikit-learn==0.19.2 numpy==1.15.1 requests==2.18.4 && \
aws s3 sync s3://tullys-models/code/ . && \
aws s3 sync s3://tullys-models/encoders/ ./encoders/ && \
python3.6 -V && \
echo running prediction_clean_folders.py && \
python3.6 prediction_clean_folders.py && \
echo running prediction_scrape.py && \
python3.6 prediction_scrape.py && \
echo running prediction_format.py && \
python3.6 prediction_format.py && \
echo running model_create_endpoint.py && \
python3.6 model_create_endpoint.py && \
echo running prediction_predict.py && \
python3.6 prediction_predict.py && \
aws s3 sync ./predictions/ s3://tullys-horse-racing-public/predictions/ ; \
echo running model_delete_endpoint.py ; \
python3.6 model_delete_endpoint.py

# aws s3 sync s3://tullys-models/encoders/ ./encoders/ --profile tully
#
# python3.6 -m pip install --upgrade awscli boto3 lxml==4.2.5 sagemaker pandas==0.23.4 scikit-learn==0.19.2 numpy==1.15.1 requests==2.20
#
# aws s3 sync ./predictions/ s3://tullys-horse-racing-public/predictions/ --profile tully
