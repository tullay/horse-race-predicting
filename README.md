aws s3 sync . s3://tullys-models/code/ --profile tully

aws s3 sync . s3://tullys-models/code2/ --profile tully

aws s3 sync s3://tullys-models/code2/ .

aws s3 sync ./data/ s3://tullys-models/data/
aws s3 sync s3://tullys-models/data/ ./data/ --profile tully

aws s3 sync ./webpage_archive/ s3://tullys-models/webpage_archive/
aws s3 sync s3://tullys-models/webpage_archive/ ./webpage_archive/ --profile tully

# Run interactivly
`docker run -it ubuntu:16.04 /bin/bash`
`export LC_ALL=en_AU.UTF-8`

# Upload image

`docker image build --tag fetchandrun .`

<!-- docker login -->
```
docker tag fetchandrun tully/fetchandrun:latest
docker push tully/fetchandrun:latest
```

export AWS_DEFAULT_PROFILE=tully

aws s3 cp config.yaml s3://tullys-models/code/config.yaml --profile tully
