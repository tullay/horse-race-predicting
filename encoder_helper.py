import pickle
from datetime import datetime
import os

def save_label_encoder(model_id ,label_encoder, feature_name):
    print('Saving Encoder for [{}] at [{}]'.format(feature_name, model_id))
    # numpy.save('encoders/{}/{}.npy'.format(datetime.strftime(datetime.now(), '%Y-%m-%d-%H-%M-%S'), feature_name), label_encoder.classes_)
    directory = 'encoders/{}/'.format(model_id)
    os.makedirs(directory, exist_ok=True)
    with open('encoders/{}/{}.pickle'.format(model_id, feature_name), 'wb') as f:
        # Pickle the 'data' dictionary using the highest protocol available.
        pickle.dump(label_encoder, f, pickle.HIGHEST_PROTOCOL)


def load_label_encoder(path):
    print('Loading Encoder at [{}]'.format(path))
    with open(path, 'rb') as f:
        label_encoder = pickle.load(f)
    return label_encoder
