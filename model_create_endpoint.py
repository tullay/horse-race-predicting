import boto3
from time import gmtime, strftime
from sagemaker.amazon.amazon_estimator import get_image_uri
import time
import os
import yaml

role = 'arn:aws:iam::928290690802:role/service-role/AmazonSageMaker-ExecutionRole-20180413T170070'
region = 'ap-southeast-2'

bucket='tullys-models' # put your s3 bucket name here, and create s3 bucket
prefix = 'sagemaker/xgboost-horseracing'
# customize to your bucket where you have stored the data
bucket_path = 'https://s3-{}.amazonaws.com/{}'.format(region,bucket)

FILE_TEST = 'out/my_csv_winner_with_zeros.csv.validation'


with open('config.yaml', 'r') as file:
    config = yaml.load(file)
model_id = config['model_id']

if os.path.exists(os.path.join(os.path.expanduser('~'), '.aws')):
    boto3.setup_default_session(profile_name='tully')

client = boto3.client('sagemaker', region_name=region)

container = get_image_uri(region, 'xgboost')
endpoint_config_name = model_id

###
# Create Model + Endpoint
###
model_name=model_id + '-model'
print(model_name)

info = client.describe_training_job(TrainingJobName=model_id)
model_data = info['ModelArtifacts']['S3ModelArtifacts']
print(model_data)

primary_container = {
    'Image': container,
    'ModelDataUrl': model_data
}

try:
    create_model_response = client.create_model(
        ModelName = model_name,
        ExecutionRoleArn = role,
        PrimaryContainer = primary_container)

    print(create_model_response['ModelArn'])
except Exception as e:
    print(e)

from time import gmtime, strftime

try:
    print(endpoint_config_name)
    create_endpoint_config_response = client.create_endpoint_config(
        EndpointConfigName = endpoint_config_name,
        ProductionVariants=[{
            'InstanceType':'ml.t2.medium',
            'InitialVariantWeight':1,
            'InitialInstanceCount':1,
            'ModelName':model_name,
            'VariantName':'AllTraffic'}])

    print("Endpoint Config Arn: " + create_endpoint_config_response['EndpointConfigArn'])
except Exception as e:
    print(e)
###
# Create endpoint
###
import time

print(model_id)
create_endpoint_response = client.create_endpoint(
    EndpointName=model_id,
    EndpointConfigName=endpoint_config_name)
print(create_endpoint_response['EndpointArn'])

###
# Test
###
resp = client.describe_endpoint(EndpointName=model_id)
status = resp['EndpointStatus']
print("Status: " + status)

while status=='Creating':
    time.sleep(60)
    resp = client.describe_endpoint(EndpointName=model_id)
    status = resp['EndpointStatus']
    print("Status: " + status)

print("Arn: " + resp['EndpointArn'])
print("Status: " + status)
